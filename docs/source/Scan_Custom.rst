Custom
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Scan.Custom.CustomCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scan.custom.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scan_Custom_Entry.rst
	Scan_Custom_ImportPy.rst