Array
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:ARRay:NX
	single: ANTenna:MODel:ARRay:NZ
	single: ANTenna:MODel:ARRay:RESolution
	single: ANTenna:MODel:ARRay:XDIStance
	single: ANTenna:MODel:ARRay:ZDIStance

.. code-block:: python

	ANTenna:MODel:ARRay:NX
	ANTenna:MODel:ARRay:NZ
	ANTenna:MODel:ARRay:RESolution
	ANTenna:MODel:ARRay:XDIStance
	ANTenna:MODel:ARRay:ZDIStance



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Array.ArrayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.array.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_Array_Cosn.rst
	Antenna_Model_Array_Distribution.rst
	Antenna_Model_Array_Element.rst
	Antenna_Model_Array_Pedestal.rst