Raster
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:RASTer:BARS
	single: SCAN:RASTer:BARTranstime
	single: SCAN:RASTer:BARWidth
	single: SCAN:RASTer:DIRection
	single: SCAN:RASTer:FLYBack
	single: SCAN:RASTer:PALMer
	single: SCAN:RASTer:PRATe
	single: SCAN:RASTer:PSQuint
	single: SCAN:RASTer:RATE
	single: SCAN:RASTer:RETRace
	single: SCAN:RASTer:REWind
	single: SCAN:RASTer:UNIDirection
	single: SCAN:RASTer:WIDTh

.. code-block:: python

	SCAN:RASTer:BARS
	SCAN:RASTer:BARTranstime
	SCAN:RASTer:BARWidth
	SCAN:RASTer:DIRection
	SCAN:RASTer:FLYBack
	SCAN:RASTer:PALMer
	SCAN:RASTer:PRATe
	SCAN:RASTer:PSQuint
	SCAN:RASTer:RATE
	SCAN:RASTer:RETRace
	SCAN:RASTer:REWind
	SCAN:RASTer:UNIDirection
	SCAN:RASTer:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Scan.Raster.RasterCls
	:members:
	:undoc-members:
	:noindex: