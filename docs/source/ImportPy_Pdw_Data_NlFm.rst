NlFm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:NLFM:CUBic
	single: IMPort:PDW:DATA:NLFM:LINear
	single: IMPort:PDW:DATA:NLFM:QUADratic

.. code-block:: python

	IMPort:PDW:DATA:NLFM:CUBic
	IMPort:PDW:DATA:NLFM:LINear
	IMPort:PDW:DATA:NLFM:QUADratic



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.NlFm.NlFmCls
	:members:
	:undoc-members:
	:noindex: