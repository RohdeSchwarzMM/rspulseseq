Pw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:BEMitter:PW:MAXimum
	single: WAVeform:BEMitter:PW:MINimum

.. code-block:: python

	WAVeform:BEMitter:PW:MAXimum
	WAVeform:BEMitter:PW:MINimum



.. autoclass:: RsPulseSeq.Implementations.Waveform.Bemitter.Pw.PwCls
	:members:
	:undoc-members:
	:noindex: