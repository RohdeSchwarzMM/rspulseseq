Arb
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.output.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Output_Arb_Details.rst