Scenario
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Cpanel.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.cpanel.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Cpanel_Scenario_Current.rst
	Cpanel_Scenario_Deployed.rst