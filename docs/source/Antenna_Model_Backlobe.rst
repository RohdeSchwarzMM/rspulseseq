Backlobe
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:BACKlobe:ATTenuation
	single: ANTenna:MODel:BACKlobe:ENABle
	single: ANTenna:MODel:BACKlobe:TYPE

.. code-block:: python

	ANTenna:MODel:BACKlobe:ATTenuation
	ANTenna:MODel:BACKlobe:ENABle
	ANTenna:MODel:BACKlobe:TYPE



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Backlobe.BacklobeCls
	:members:
	:undoc-members:
	:noindex: