Marker
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:MARKer:AUTO
	single: SCENario:LOCalized:MARKer:FALL
	single: SCENario:LOCalized:MARKer:FORCe
	single: SCENario:LOCalized:MARKer:GATE
	single: SCENario:LOCalized:MARKer:POST
	single: SCENario:LOCalized:MARKer:PRE
	single: SCENario:LOCalized:MARKer:RISE
	single: SCENario:LOCalized:MARKer:WIDTh

.. code-block:: python

	SCENario:LOCalized:MARKer:AUTO
	SCENario:LOCalized:MARKer:FALL
	SCENario:LOCalized:MARKer:FORCe
	SCENario:LOCalized:MARKer:GATE
	SCENario:LOCalized:MARKer:POST
	SCENario:LOCalized:MARKer:PRE
	SCENario:LOCalized:MARKer:RISE
	SCENario:LOCalized:MARKer:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Marker_Time.rst