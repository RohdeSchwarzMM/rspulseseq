Fm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:FM:DEViation
	single: PULSe:MOP:FM:FREQuency

.. code-block:: python

	PULSe:MOP:FM:DEViation
	PULSe:MOP:FM:FREQuency



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Fm.FmCls
	:members:
	:undoc-members:
	:noindex: