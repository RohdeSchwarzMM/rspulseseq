Mchg
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:MCHG:CLEar
	single: SCENario:CEMit:MCHG:COUNt
	single: SCENario:CEMit:MCHG:DELete
	single: SCENario:CEMit:MCHG:SELect
	single: SCENario:CEMit:MCHG:STARt
	single: SCENario:CEMit:MCHG:STATe
	single: SCENario:CEMit:MCHG:STOP

.. code-block:: python

	SCENario:CEMit:MCHG:CLEar
	SCENario:CEMit:MCHG:COUNt
	SCENario:CEMit:MCHG:DELete
	SCENario:CEMit:MCHG:SELect
	SCENario:CEMit:MCHG:STARt
	SCENario:CEMit:MCHG:STATe
	SCENario:CEMit:MCHG:STOP



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Mchg.MchgCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cemit.mchg.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cemit_Mchg_Add.rst