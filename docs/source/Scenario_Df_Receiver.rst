Receiver
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:RECeiver:HEIGht
	single: SCENario:DF:RECeiver:LATitude
	single: SCENario:DF:RECeiver:LONGitude
	single: SCENario:DF:RECeiver

.. code-block:: python

	SCENario:DF:RECeiver:HEIGht
	SCENario:DF:RECeiver:LATitude
	SCENario:DF:RECeiver:LONGitude
	SCENario:DF:RECeiver



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Receiver.ReceiverCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.receiver.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Receiver_Direction.rst
	Scenario_Df_Receiver_Movement.rst