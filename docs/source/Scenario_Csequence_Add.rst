Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:CSEQuence:ADD

.. code-block:: python

	SCENario:CSEQuence:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Csequence.Add.AddCls
	:members:
	:undoc-members:
	:noindex: