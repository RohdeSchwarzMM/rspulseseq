Receiver
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: RECeiver:CATalog
	single: RECeiver:COMMent
	single: RECeiver:CREate
	single: RECeiver:MODel
	single: RECeiver:NAME
	single: RECeiver:REMove
	single: RECeiver:SELect

.. code-block:: python

	RECeiver:CATalog
	RECeiver:COMMent
	RECeiver:CREate
	RECeiver:MODel
	RECeiver:NAME
	RECeiver:REMove
	RECeiver:SELect



.. autoclass:: RsPulseSeq.Implementations.Receiver.ReceiverCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.receiver.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Receiver_Antenna.rst