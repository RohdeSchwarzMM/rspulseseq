Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:MCHG:ADD

.. code-block:: python

	SCENario:DF:MCHG:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Mchg.Add.AddCls
	:members:
	:undoc-members:
	:noindex: