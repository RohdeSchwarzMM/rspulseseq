Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:MCHG:ADD

.. code-block:: python

	SCENario:LOCalized:MCHG:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Mchg.Add.AddCls
	:members:
	:undoc-members:
	:noindex: