Exec
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCAN:CUSTom:IMPort:EXEC

.. code-block:: python

	SCAN:CUSTom:IMPort:EXEC



.. autoclass:: RsPulseSeq.Implementations.Scan.Custom.ImportPy.Exec.ExecCls
	:members:
	:undoc-members:
	:noindex: