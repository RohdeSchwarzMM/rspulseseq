Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ASSignment:GENerator:PATH:ANTenna:ADD

.. code-block:: python

	ASSignment:GENerator:PATH:ANTenna:ADD



.. autoclass:: RsPulseSeq.Implementations.Assignment.Generator.Path.Antenna.Add.AddCls
	:members:
	:undoc-members:
	:noindex: