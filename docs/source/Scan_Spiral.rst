Spiral
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:SPIRal:PALMer
	single: SCAN:SPIRal:PRATe
	single: SCAN:SPIRal:PSQuint
	single: SCAN:SPIRal:RETRace
	single: SCAN:SPIRal:ROTation
	single: SCAN:SPIRal:ROUNds
	single: SCAN:SPIRal:RTIMe
	single: SCAN:SPIRal:STEP
	single: SCAN:SPIRal:UNIDirection

.. code-block:: python

	SCAN:SPIRal:PALMer
	SCAN:SPIRal:PRATe
	SCAN:SPIRal:PSQuint
	SCAN:SPIRal:RETRace
	SCAN:SPIRal:ROTation
	SCAN:SPIRal:ROUNds
	SCAN:SPIRal:RTIMe
	SCAN:SPIRal:STEP
	SCAN:SPIRal:UNIDirection



.. autoclass:: RsPulseSeq.Implementations.Scan.Spiral.SpiralCls
	:members:
	:undoc-members:
	:noindex: