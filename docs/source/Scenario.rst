Scenario
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CATalog
	single: SCENario:COMMent
	single: SCENario:CREate
	single: SCENario:ID
	single: SCENario:NAME
	single: SCENario:REMove
	single: SCENario:SANitize
	single: SCENario:SELect
	single: SCENario:STARt
	single: SCENario:STATe
	single: SCENario:STOP
	single: SCENario:TYPE
	single: SCENario:WAVeform

.. code-block:: python

	SCENario:CATalog
	SCENario:COMMent
	SCENario:CREate
	SCENario:ID
	SCENario:NAME
	SCENario:REMove
	SCENario:SANitize
	SCENario:SELect
	SCENario:STARt
	SCENario:STATe
	SCENario:STOP
	SCENario:TYPE
	SCENario:WAVeform



.. autoclass:: RsPulseSeq.Implementations.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cache.rst
	Scenario_Calculate.rst
	Scenario_Cemit.rst
	Scenario_Cpdw.rst
	Scenario_Csequence.rst
	Scenario_Destination.rst
	Scenario_Df.rst
	Scenario_Emitter.rst
	Scenario_Generator.rst
	Scenario_IlCache.rst
	Scenario_Interleave.rst
	Scenario_Localized.rst
	Scenario_Output.rst
	Scenario_Pdw.rst
	Scenario_Sequence.rst
	Scenario_Trigger.rst
	Scenario_Volatile.rst