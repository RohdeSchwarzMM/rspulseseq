Assignment
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Assignment.AssignmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Antennas.rst
	Assignment_Destination.rst
	Assignment_Emitters.rst
	Assignment_Generator.rst
	Assignment_Group.rst