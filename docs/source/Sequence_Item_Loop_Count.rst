Count
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:LOOP:COUNt:FIXed
	single: SEQuence:ITEM:LOOP:COUNt:MAXimum
	single: SEQuence:ITEM:LOOP:COUNt:MINimum
	single: SEQuence:ITEM:LOOP:COUNt:STEP

.. code-block:: python

	SEQuence:ITEM:LOOP:COUNt:FIXed
	SEQuence:ITEM:LOOP:COUNt:MAXimum
	SEQuence:ITEM:LOOP:COUNt:MINimum
	SEQuence:ITEM:LOOP:COUNt:STEP



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Loop.Count.CountCls
	:members:
	:undoc-members:
	:noindex: