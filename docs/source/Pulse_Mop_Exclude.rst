Exclude
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:EXCLude:ENABle
	single: PULSe:MOP:EXCLude:MODE

.. code-block:: python

	PULSe:MOP:EXCLude:ENABle
	PULSe:MOP:EXCLude:MODE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Exclude.ExcludeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.exclude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Exclude_Level.rst
	Pulse_Mop_Exclude_Time.rst