Module
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PLUGin:MODule:AUTHor
	single: PLUGin:MODule:COMMent
	single: PLUGin:MODule:DATA
	single: PLUGin:MODule:NAME
	single: PLUGin:MODule:TYPE
	single: PLUGin:MODule:VERSion

.. code-block:: python

	PLUGin:MODule:AUTHor
	PLUGin:MODule:COMMent
	PLUGin:MODule:DATA
	PLUGin:MODule:NAME
	PLUGin:MODule:TYPE
	PLUGin:MODule:VERSion



.. autoclass:: RsPulseSeq.Implementations.Plugin.Module.ModuleCls
	:members:
	:undoc-members:
	:noindex: