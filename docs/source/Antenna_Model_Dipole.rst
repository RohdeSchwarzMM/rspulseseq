Dipole
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ANTenna:MODel:DIPole:RESolution

.. code-block:: python

	ANTenna:MODel:DIPole:RESolution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Dipole.DipoleCls
	:members:
	:undoc-members:
	:noindex: