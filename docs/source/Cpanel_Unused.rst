Unused
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CPANel:UNUSed:ALL
	single: CPANel:UNUSed:FREQuency
	single: CPANel:UNUSed:LEVel
	single: CPANel:UNUSed:LIST
	single: CPANel:UNUSed:SELect
	single: CPANel:UNUSed:STATe

.. code-block:: python

	CPANel:UNUSed:ALL
	CPANel:UNUSed:FREQuency
	CPANel:UNUSed:LEVel
	CPANel:UNUSed:LIST
	CPANel:UNUSed:SELect
	CPANel:UNUSed:STATe



.. autoclass:: RsPulseSeq.Implementations.Cpanel.Unused.UnusedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.cpanel.unused.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Cpanel_Unused_Path.rst