Interleaving
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:INTerleaving:MODE
	single: SCENario:DF:INTerleaving:FREQagility
	single: SCENario:DF:INTerleaving

.. code-block:: python

	SCENario:DF:INTerleaving:MODE
	SCENario:DF:INTerleaving:FREQagility
	SCENario:DF:INTerleaving



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Interleaving.InterleavingCls
	:members:
	:undoc-members:
	:noindex: