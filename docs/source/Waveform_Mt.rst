Mt
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:MT:COUNt
	single: WAVeform:MT:SPACing

.. code-block:: python

	WAVeform:MT:COUNt
	WAVeform:MT:SPACing



.. autoclass:: RsPulseSeq.Implementations.Waveform.Mt.MtCls
	:members:
	:undoc-members:
	:noindex: