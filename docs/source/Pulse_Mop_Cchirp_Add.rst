Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:CCHirp:ADD

.. code-block:: python

	PULSe:MOP:CCHirp:ADD



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Cchirp.Add.AddCls
	:members:
	:undoc-members:
	:noindex: