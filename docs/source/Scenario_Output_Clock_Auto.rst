Auto
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:OUTPut:CLOCk:AUTO:BORDer
	single: SCENario:OUTPut:CLOCk:AUTO:OVERsampling

.. code-block:: python

	SCENario:OUTPut:CLOCk:AUTO:BORDer
	SCENario:OUTPut:CLOCk:AUTO:OVERsampling



.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.Clock.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex: