Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:CEMit:GROup:ADD

.. code-block:: python

	SCENario:CEMit:GROup:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Group.Add.AddCls
	:members:
	:undoc-members:
	:noindex: