Operation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STATus:OPERation:CONDition
	single: STATus:OPERation:ENABle
	single: STATus:OPERation:EVENt
	single: STATus:OPERation:NTRansition
	single: STATus:OPERation:PTRansition

.. code-block:: python

	STATus:OPERation:CONDition
	STATus:OPERation:ENABle
	STATus:OPERation:EVENt
	STATus:OPERation:NTRansition
	STATus:OPERation:PTRansition



.. autoclass:: RsPulseSeq.Implementations.Status.Operation.OperationCls
	:members:
	:undoc-members:
	:noindex: