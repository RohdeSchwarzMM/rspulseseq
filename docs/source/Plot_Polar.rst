Polar
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PLOT:POLar:CUT
	single: PLOT:POLar:TYPE

.. code-block:: python

	PLOT:POLar:CUT
	PLOT:POLar:TYPE



.. autoclass:: RsPulseSeq.Implementations.Plot.Polar.PolarCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.plot.polar.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Plot_Polar_Log.rst