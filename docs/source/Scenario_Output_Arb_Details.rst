Details
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:OUTPut:ARB:DETails:ALBS
	single: SCENario:OUTPut:ARB:DETails:TRUNcate

.. code-block:: python

	SCENario:OUTPut:ARB:DETails:ALBS
	SCENario:OUTPut:ARB:DETails:TRUNcate



.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.Arb.Details.DetailsCls
	:members:
	:undoc-members:
	:noindex: