Subitem
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:SUBitem:CURRent
	single: SCENario:LOCalized:SUBitem:SELect

.. code-block:: python

	SCENario:LOCalized:SUBitem:CURRent
	SCENario:LOCalized:SUBitem:SELect



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Subitem.SubitemCls
	:members:
	:undoc-members:
	:noindex: