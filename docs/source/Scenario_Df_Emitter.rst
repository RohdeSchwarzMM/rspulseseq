Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:EMITter:ENABle
	single: SCENario:DF:EMITter

.. code-block:: python

	SCENario:DF:EMITter:ENABle
	SCENario:DF:EMITter



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Emitter_Mode.rst
	Scenario_Df_Emitter_State.rst