AmStep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:AMSTep:CLEar
	single: PULSe:MOP:AMSTep:COUNt
	single: PULSe:MOP:AMSTep:DELete
	single: PULSe:MOP:AMSTep:DURation
	single: PULSe:MOP:AMSTep:INSert
	single: PULSe:MOP:AMSTep:LEVel
	single: PULSe:MOP:AMSTep:SELect

.. code-block:: python

	PULSe:MOP:AMSTep:CLEar
	PULSe:MOP:AMSTep:COUNt
	PULSe:MOP:AMSTep:DELete
	PULSe:MOP:AMSTep:DURation
	PULSe:MOP:AMSTep:INSert
	PULSe:MOP:AMSTep:LEVel
	PULSe:MOP:AMSTep:SELect



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.AmStep.AmStepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.amStep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_AmStep_Add.rst