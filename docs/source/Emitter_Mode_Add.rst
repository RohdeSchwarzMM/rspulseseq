Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: EMITter:MODE:ADD

.. code-block:: python

	EMITter:MODE:ADD



.. autoclass:: RsPulseSeq.Implementations.Emitter.Mode.Add.AddCls
	:members:
	:undoc-members:
	:noindex: