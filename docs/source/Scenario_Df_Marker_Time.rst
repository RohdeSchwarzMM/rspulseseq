Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:MARKer:TIME:POST
	single: SCENario:DF:MARKer:TIME:PRE

.. code-block:: python

	SCENario:DF:MARKer:TIME:POST
	SCENario:DF:MARKer:TIME:PRE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Marker.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: