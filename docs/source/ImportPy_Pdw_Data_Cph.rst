Cph
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:CPH:CHIPcount
	single: IMPort:PDW:DATA:CPH:VALues

.. code-block:: python

	IMPort:PDW:DATA:CPH:CHIPcount
	IMPort:PDW:DATA:CPH:VALues



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.Cph.CphCls
	:members:
	:undoc-members:
	:noindex: