Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:ADD

.. code-block:: python

	SCENario:DF:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Add.AddCls
	:members:
	:undoc-members:
	:noindex: