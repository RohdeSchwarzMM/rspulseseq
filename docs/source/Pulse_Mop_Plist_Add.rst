Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:PLISt:ADD

.. code-block:: python

	PULSe:MOP:PLISt:ADD



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Plist.Add.AddCls
	:members:
	:undoc-members:
	:noindex: