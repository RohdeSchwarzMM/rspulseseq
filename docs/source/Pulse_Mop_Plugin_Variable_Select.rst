Select
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:PLUGin:VARiable:SELect:ID
	single: PULSe:MOP:PLUGin:VARiable:SELect

.. code-block:: python

	PULSe:MOP:PLUGin:VARiable:SELect:ID
	PULSe:MOP:PLUGin:VARiable:SELect



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Plugin.Variable.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: