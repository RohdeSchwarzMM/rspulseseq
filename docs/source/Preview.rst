Preview
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREView:POSition

.. code-block:: python

	PREView:POSition



.. autoclass:: RsPulseSeq.Implementations.Preview.PreviewCls
	:members:
	:undoc-members:
	:noindex: