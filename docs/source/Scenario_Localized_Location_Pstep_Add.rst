Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:LOCation:PSTep:ADD

.. code-block:: python

	SCENario:LOCalized:LOCation:PSTep:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Location.Pstep.Add.AddCls
	:members:
	:undoc-members:
	:noindex: