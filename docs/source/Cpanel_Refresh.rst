Refresh
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CPANel:REFResh

.. code-block:: python

	CPANel:REFResh



.. autoclass:: RsPulseSeq.Implementations.Cpanel.Refresh.RefreshCls
	:members:
	:undoc-members:
	:noindex: