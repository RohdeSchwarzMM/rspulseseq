Variable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:PDW:PLUGin:VARiable:CATalog
	single: SCENario:PDW:PLUGin:VARiable:RESet
	single: SCENario:PDW:PLUGin:VARiable:VALue

.. code-block:: python

	SCENario:PDW:PLUGin:VARiable:CATalog
	SCENario:PDW:PLUGin:VARiable:RESet
	SCENario:PDW:PLUGin:VARiable:VALue



.. autoclass:: RsPulseSeq.Implementations.Scenario.Pdw.Plugin.Variable.VariableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.pdw.plugin.variable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Pdw_Plugin_Variable_Select.rst