Setup
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SETup:ADD
	single: SETup:DELete
	single: SETup:EXPort
	single: SETup:IMPort
	single: SETup:BBSYnc
	single: SETup:COUNt
	single: SETup:LIST
	single: SETup:SELect

.. code-block:: python

	SETup:ADD
	SETup:DELete
	SETup:EXPort
	SETup:IMPort
	SETup:BBSYnc
	SETup:COUNt
	SETup:LIST
	SETup:SELect



.. autoclass:: RsPulseSeq.Implementations.Setup.SetupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.setup.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Setup_HigHq.rst
	Setup_Locpl.rst
	Setup_Pmod.rst
	Setup_RfAlign.rst