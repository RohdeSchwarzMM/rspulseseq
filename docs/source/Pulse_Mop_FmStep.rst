FmStep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:FMSTep:CLEar
	single: PULSe:MOP:FMSTep:COUNt
	single: PULSe:MOP:FMSTep:DELete
	single: PULSe:MOP:FMSTep:DURation
	single: PULSe:MOP:FMSTep:FREQuency
	single: PULSe:MOP:FMSTep:INSert
	single: PULSe:MOP:FMSTep:SELect

.. code-block:: python

	PULSe:MOP:FMSTep:CLEar
	PULSe:MOP:FMSTep:COUNt
	PULSe:MOP:FMSTep:DELete
	PULSe:MOP:FMSTep:DURation
	PULSe:MOP:FMSTep:FREQuency
	PULSe:MOP:FMSTep:INSert
	PULSe:MOP:FMSTep:SELect



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.FmStep.FmStepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.fmStep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_FmStep_Add.rst