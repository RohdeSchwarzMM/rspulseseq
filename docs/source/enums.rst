Enums
=========

AmType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AmType.LSB
	# All values (4x):
	LSB | SB | STD | USB

AntennaModel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AntennaModel.ARRay
	# Last value:
	value = enums.AntennaModel.USER
	# All values (12x):
	ARRay | CARDoid | CARRay | COSecant | CUSTom | DIPole | GAUSsian | HORN
	PARabolic | PLUGin | SINC | USER

AntennaModelArray
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AntennaModelArray.COSine
	# All values (8x):
	COSine | COSN | CSQuared | HAMMing | HANN | PARabolic | TRIangular | UNIForm

Attitude
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Attitude.CONStant
	# All values (3x):
	CONStant | MOTion | WAYPoint

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

Azimuth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Azimuth.BEARing
	# All values (2x):
	BEARing | RX

BarkerCode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BarkerCode.R11
	# Last value:
	value = enums.BarkerCode.R7
	# All values (9x):
	R11 | R13 | R2A | R2B | R3 | R4A | R4B | R5
	R7

BaseDomain
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BaseDomain.PULSe
	# All values (2x):
	PULSe | TIME

BaseDomainB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BaseDomainB.LENGth
	# All values (2x):
	LENGth | TIME

BbSync
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbSync.CTRigger
	# All values (3x):
	CTRigger | TRIGger | UNSYnc

BlockSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BlockSize._16K
	# All values (6x):
	_16K | _1M | _2M | _32K | _4M | _64K

BlType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BlType.MIRRor
	# All values (2x):
	MIRRor | OMNidirect

BpskTtype
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BpskTtype.COSine
	# All values (2x):
	COSine | LINear

BpskType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BpskType.CONStant
	# All values (2x):
	CONStant | STANdard

BufferSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BufferSize._128M
	# All values (6x):
	_128M | _16M | _1G | _256M | _512M | _64M

ChirpType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChirpType.DOWN
	# All values (5x):
	DOWN | PIECewise | SINE | TRIangular | UP

CircularMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CircularMode.RPM
	# All values (2x):
	RPM | SEC

Coding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Coding.DGRay
	# All values (4x):
	DGRay | DIFFerential | GRAY | NONE

Complexity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Complexity.DIRection
	# All values (3x):
	DIRection | EMITter | PTRain

Condition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Condition.EQUal
	# All values (4x):
	EQUal | GREater | NOTequal | SMALler

DataMop
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DataMop.AM
	# Last value:
	value = enums.DataMop.TFM
	# All values (20x):
	AM | ASK | BKR11 | BKR13 | BKR2a | BKR2b | BKR3 | BKR4a
	BKR4b | BKR5 | BKR7 | CPH | CW | FM | FSK | LFM
	NLFM | PLFM | PSK | TFM

DataUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataUnit.DB
	# All values (3x):
	DB | VOLTage | WATTs

DfType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DfType.BACKground
	# All values (4x):
	BACKground | EMITter | PLATform | WAVeform

EnvelopeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnvelopeMode.DATA
	# All values (2x):
	DATA | EQUation

ExcMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExcMode.LEVel
	# All values (3x):
	LEVel | TIME | WIDTh

FillerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FillerMode.DURation
	# All values (2x):
	DURation | TSYNc

FillerSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FillerSignal.BLANk
	# All values (3x):
	BLANk | CW | HOLD

FillerTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FillerTime.EQUation
	# All values (2x):
	EQUation | FIXed

FilterType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FilterType.COS
	# Last value:
	value = enums.FilterType.SOQPsk
	# All values (9x):
	COS | FSKGauss | GAUSs | LPASs | NONE | RCOS | RECTangular | SMWRect
	SOQPsk

FskType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FskType.FS16
	# All values (6x):
	FS16 | FS2 | FS32 | FS4 | FS64 | FS8

GeneratorType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorType.SGT
	# All values (8x):
	SGT | SMBB | SMBV | SMJ | SMM | SMU | SMW | SW

Geometry
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Geometry.CIRCular
	# All values (4x):
	CIRCular | HEXagonal | LINear | RECTangular

HqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HqMode.NORMal
	# All values (2x):
	NORMal | TABLe

InterleaveMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InterleaveMode.DROP
	# All values (2x):
	DROP | MERGe

Interpolation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Interpolation.LINear
	# All values (2x):
	LINear | NONE

IpmMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpmMode.INDividual
	# All values (2x):
	INDividual | SAME

IpmPlotView
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpmPlotView.HISTogram
	# All values (2x):
	HISTogram | TIMeseries

IpmType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.IpmType.BINomial
	# Last value:
	value = enums.IpmType.WAVeform
	# All values (10x):
	BINomial | EQUation | LIST | PLUGin | RANDom | RLISt | RSTep | SHAPe
	STEPs | WAVeform

ItemPattern
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ItemPattern.ALT
	# Last value:
	value = enums.ItemPattern.ZERO
	# All values (12x):
	ALT | ONE | R11 | R13 | R2A | R2B | R3 | R4A
	R4B | R5 | R7 | ZERO

ItemType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ItemType.FILLer
	# All values (6x):
	FILLer | LOOP | OVL | PULSe | SUBSequence | WAVeform

ItemTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ItemTypeB.PATTern
	# All values (3x):
	PATTern | PRBS | USER

Lattice
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Lattice.RECTangular
	# All values (2x):
	RECTangular | TRIangular

LobesCount
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LobesCount._2
	# All values (2x):
	_2 | _4

LoopType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoopType.FIXed
	# All values (2x):
	FIXed | VARiable

LswDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LswDirection.H
	# All values (2x):
	H | V

ModuleType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModuleType.IPM
	# All values (3x):
	IPM | MOP | REPort

MopType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MopType.AM
	# Last value:
	value = enums.MopType.QPSK
	# All values (21x):
	AM | AMSTep | ASK | BARKer | BPSK | CCHiprp | CHIRp | FM
	FMSTep | FSK | MSK | NLCHirp | NOISe | PCHirp | PLISt | PLUGin
	POLYphase | PSK8 | PWISechirp | QAM | QPSK

MovementRframe
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MovementRframe.PZ
	# All values (2x):
	PZ | WGS

MovementRmode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MovementRmode.CYCLic
	# All values (3x):
	CYCLic | ONEWay | ROUNdtrip

MovementType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MovementType.ARC
	# All values (4x):
	ARC | LINE | TRACe | WAYPoint

OutFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OutFormat.ESEQencing
	# All values (3x):
	ESEQencing | MSW | WV

PhaseMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PhaseMode.ABSolute
	# All values (3x):
	ABSolute | CONTinuous | MEMory

Pmode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Pmode.MOVing
	# All values (2x):
	MOVing | STATic

PmodeLocation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PmodeLocation.MOVing
	# All values (3x):
	MOVing | STATic | STEPs

PmodSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PmodSource.EXTernal
	# All values (3x):
	EXTernal | INTernal | OFF

PolarCut
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PolarCut.XY
	# All values (2x):
	XY | YZ

Polarization
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Polarization.CLEFt
	# All values (6x):
	CLEFt | CRIGht | HORizontal | SLEFt | SRIGht | VERTical

PolarType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PolarType.CARTesian
	# All values (2x):
	CARTesian | POLar

PolynomType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PolynomType.FRANk
	# All values (5x):
	FRANk | P1 | P2 | P3 | P4

PrbsType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrbsType.P11
	# All values (8x):
	P11 | P15 | P16 | P20 | P21 | P23 | P7 | P9

PreviewMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PreviewMode.ENVelope
	# All values (2x):
	ENVelope | MOP

PreviewMop
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PreviewMop.FREQuency
	# All values (3x):
	FREQuency | IQ | PHASe

ProgramMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProgramMode.DEMO
	# All values (3x):
	DEMO | EXPert | STANdard

Psec
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Psec.NONE
	# Last value:
	value = enums.Psec.SEC9
	# All values (18x):
	NONE | PRIMary | SEC1 | SEC10 | SEC11 | SEC12 | SEC13 | SEC14
	SEC15 | SEC16 | SEC2 | SEC3 | SEC4 | SEC5 | SEC6 | SEC7
	SEC8 | SEC9

PulseSetting
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulseSetting.GENeral
	# All values (5x):
	GENeral | LEVel | MKR | MOP | TIMing

PulseType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulseType.COSine
	# All values (4x):
	COSine | LINear | RCOSine | SQRT

PwdType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwdType.AMMos
	# All values (4x):
	AMMos | DEFault | PLUGin | TEMPlate

QamType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.QamType.Q128
	# All values (5x):
	Q128 | Q16 | Q256 | Q32 | Q64

QpskType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.QpskType.ASOQpsk
	# All values (6x):
	ASOQpsk | BSOQpsk | DQPSk | NORMal | OQPSk | TGSoqpsk

RandomDistribution
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RandomDistribution.NORMal
	# All values (3x):
	NORMal | U | UNIForm

RasterDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RasterDirection.HORizontal
	# All values (2x):
	HORizontal | VERTical

RecModel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RecModel.COMBined
	# All values (3x):
	COMBined | INTerfero | TDOA

RepeatMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepeatMode.CONTinuous
	# All values (2x):
	CONTinuous | SINGle

RepetitionType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepetitionType.DURation
	# All values (3x):
	DURation | FIXed | VARiable

Rotation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rotation.CCW
	# All values (2x):
	CCW | CW

SanitizeScenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SanitizeScenario.ALL
	# All values (3x):
	ALL | REPository | SCENario

ScanType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ScanType.CIRCular
	# Last value:
	value = enums.ScanType.SPIRal
	# All values (10x):
	CIRCular | CONical | CUSTom | HELical | LISSajous | LSW | RASTer | SECTor
	SIN | SPIRal

ScenarioType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ScenarioType.CEMitter
	# Last value:
	value = enums.ScenarioType.WAVeform
	# All values (9x):
	CEMitter | CSEQuence | DF | DYNamic | EMITter | LOCalized | PDW | SEQuence
	WAVeform

SecurityLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SecurityLevel.LEV0
	# All values (5x):
	LEV0 | LEV1 | LEV2 | LEV3 | LEV4

SequenceType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SequenceType.PULSe
	# All values (2x):
	PULSe | WAVeform

SigCont
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SigCont.COMM
	# All values (2x):
	COMM | PULSe

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SourceType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceType.PROFile
	# All values (2x):
	PROFile | VARiable

State
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.State.IDLE
	# All values (2x):
	IDLE | RUN

TargetOut
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetOut.FILE
	# All values (2x):
	FILE | INSTrument

TargetParam
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TargetParam.AMDepth
	# Last value:
	value = enums.TargetParam.WIDTh
	# All values (20x):
	AMDepth | AMFRequency | CDEViation | DELay | DROop | FALL | FMDeviation | FMFRequency
	FREQuency | FSKDeviation | LEVel | OVERshoot | PHASe | PRF | PRI | RFRequency
	RISE | RLEVel | SRATe | WIDTh

TargetType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetType.PARameter
	# All values (2x):
	PARameter | VARiable

TimeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeMode.PRF
	# All values (2x):
	PRF | PRI

TimeReference
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeReference.FULL
	# All values (3x):
	FULL | POWer | VOLTage

Units
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Units.DB
	# All values (6x):
	DB | DEGRees | HERTz | NONE | PERCent | SEConds

Vehicle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Vehicle.AIRPlane
	# All values (5x):
	AIRPlane | LVEHicle | RECeiver | SHIP | STATionary

VehicleMovement
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VehicleMovement.AIRPlane
	# All values (6x):
	AIRPlane | CAR | DEFault | LVEHicle | SHIP | STATionary

ViewCount
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ViewCount._100
	# All values (8x):
	_100 | _1000 | _10000 | _100000 | _50 | _500 | _5000 | _50000

ViewXode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ViewXode.SAMPles
	# All values (2x):
	SAMPles | TIME

WaveformShape
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WaveformShape.RAMP
	# All values (3x):
	RAMP | SINE | TRIangular

WaveformType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.WaveformType.AIF
	# Last value:
	value = enums.WaveformType.WAVeform
	# All values (10x):
	AIF | APDW | BEMitter | CW | IQDW | MT | NOISe | PDW
	USER | WAVeform

Ymode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Ymode.FREQuency
	# All values (7x):
	FREQuency | IQ | MAGDb | MAGV | MAGW | PAV | PHASe

