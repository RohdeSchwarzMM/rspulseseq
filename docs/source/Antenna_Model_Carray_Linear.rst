Linear
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:LINear:DISTance
	single: ANTenna:MODel:CARRay:LINear:N

.. code-block:: python

	ANTenna:MODel:CARRay:LINear:DISTance
	ANTenna:MODel:CARRay:LINear:N



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Linear.LinearCls
	:members:
	:undoc-members:
	:noindex: