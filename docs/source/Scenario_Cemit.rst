Cemit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:ALIas
	single: SCENario:CEMit:CLEar
	single: SCENario:CEMit:CURRent
	single: SCENario:CEMit:DELete
	single: SCENario:CEMit:ENABle
	single: SCENario:CEMit:FQOFfset
	single: SCENario:CEMit:FREQuency
	single: SCENario:CEMit:LDELay
	single: SCENario:CEMit:LEVel
	single: SCENario:CEMit:LVABs
	single: SCENario:CEMit:PRIority
	single: SCENario:CEMit:SCNDelay
	single: SCENario:CEMit:SELect
	single: SCENario:CEMit:THReshold

.. code-block:: python

	SCENario:CEMit:ALIas
	SCENario:CEMit:CLEar
	SCENario:CEMit:CURRent
	SCENario:CEMit:DELete
	SCENario:CEMit:ENABle
	SCENario:CEMit:FQOFfset
	SCENario:CEMit:FREQuency
	SCENario:CEMit:LDELay
	SCENario:CEMit:LEVel
	SCENario:CEMit:LVABs
	SCENario:CEMit:PRIority
	SCENario:CEMit:SCNDelay
	SCENario:CEMit:SELect
	SCENario:CEMit:THReshold



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.CemitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cemit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cemit_Add.rst
	Scenario_Cemit_Direction.rst
	Scenario_Cemit_Emitter.rst
	Scenario_Cemit_Group.rst
	Scenario_Cemit_Interleaving.rst
	Scenario_Cemit_Marker.rst
	Scenario_Cemit_Mchg.rst