Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SEQuence:ITEM:FREQuency:OFFSet

.. code-block:: python

	SEQuence:ITEM:FREQuency:OFFSet



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: