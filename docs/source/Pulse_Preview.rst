Preview
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:PREView:MODE
	single: PULSe:PREView:MOP

.. code-block:: python

	PULSe:PREView:MODE
	PULSe:PREView:MOP



.. autoclass:: RsPulseSeq.Implementations.Pulse.Preview.PreviewCls
	:members:
	:undoc-members:
	:noindex: