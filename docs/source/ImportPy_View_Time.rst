Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IMPort:VIEW:TIME:STARt

.. code-block:: python

	IMPort:VIEW:TIME:STARt



.. autoclass:: RsPulseSeq.Implementations.ImportPy.View.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: