Source
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:IPM:SOURce:TYPE
	single: SEQuence:ITEM:IPM:SOURce:VARiable
	single: SEQuence:ITEM:IPM:SOURce

.. code-block:: python

	SEQuence:ITEM:IPM:SOURce:TYPE
	SEQuence:ITEM:IPM:SOURce:VARiable
	SEQuence:ITEM:IPM:SOURce



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Ipm.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: