Accept
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:SETTings:ACCept

.. code-block:: python

	PROGram:SETTings:ACCept



.. autoclass:: RsPulseSeq.Implementations.Program.Settings.Accept.AcceptCls
	:members:
	:undoc-members:
	:noindex: