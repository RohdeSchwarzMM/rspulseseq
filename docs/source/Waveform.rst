Waveform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:CATalog
	single: WAVeform:COMMent
	single: WAVeform:CREate
	single: WAVeform:NAME
	single: WAVeform:REMove
	single: WAVeform:SELect
	single: WAVeform:SIGCont
	single: WAVeform:TYPE

.. code-block:: python

	WAVeform:CATalog
	WAVeform:COMMent
	WAVeform:CREate
	WAVeform:NAME
	WAVeform:REMove
	WAVeform:SELect
	WAVeform:SIGCont
	WAVeform:TYPE



.. autoclass:: RsPulseSeq.Implementations.Waveform.WaveformCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.waveform.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Waveform_Bemitter.rst
	Waveform_Iq.rst
	Waveform_Level.rst
	Waveform_Mt.rst
	Waveform_Noise.rst
	Waveform_Pdw.rst
	Waveform_View.rst
	Waveform_Waveform.rst