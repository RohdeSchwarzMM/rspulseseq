Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:EMITter:MODE:BEAM
	single: SCENario:LOCalized:EMITter:MODE:TRACkrec
	single: SCENario:LOCalized:EMITter:MODE

.. code-block:: python

	SCENario:LOCalized:EMITter:MODE:BEAM
	SCENario:LOCalized:EMITter:MODE:TRACkrec
	SCENario:LOCalized:EMITter:MODE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Emitter.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: