View
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:VIEW:XMODe
	single: WAVeform:VIEW:YMODe

.. code-block:: python

	WAVeform:VIEW:XMODe
	WAVeform:VIEW:YMODe



.. autoclass:: RsPulseSeq.Implementations.Waveform.View.ViewCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.waveform.view.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Waveform_View_Get.rst
	Waveform_View_Open.rst
	Waveform_View_Zoom.rst