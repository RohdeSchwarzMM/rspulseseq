Interleaving
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:INTerleaving:MODE
	single: SCENario:CEMit:INTerleaving:FREQagility
	single: SCENario:CEMit:INTerleaving

.. code-block:: python

	SCENario:CEMit:INTerleaving:MODE
	SCENario:CEMit:INTerleaving:FREQagility
	SCENario:CEMit:INTerleaving



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Interleaving.InterleavingCls
	:members:
	:undoc-members:
	:noindex: