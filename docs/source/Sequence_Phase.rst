Phase
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SEQuence:PHASe:MODE

.. code-block:: python

	SEQuence:PHASe:MODE



.. autoclass:: RsPulseSeq.Implementations.Sequence.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex: