Scenario
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:OUTPut:MARKer:SCENario:DURation
	single: SCENario:OUTPut:MARKer:SCENario:ENABle

.. code-block:: python

	SCENario:OUTPut:MARKer:SCENario:DURation
	SCENario:OUTPut:MARKer:SCENario:ENABle



.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.Marker.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex: