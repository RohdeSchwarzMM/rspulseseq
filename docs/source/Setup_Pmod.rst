Pmod
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SETup:PMOD:DIRection
	single: SETup:PMOD:ENABle

.. code-block:: python

	SETup:PMOD:DIRection
	SETup:PMOD:ENABle



.. autoclass:: RsPulseSeq.Implementations.Setup.Pmod.PmodCls
	:members:
	:undoc-members:
	:noindex: