Destination
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DESTination:CLEar
	single: SCENario:DESTination

.. code-block:: python

	SCENario:DESTination:CLEar
	SCENario:DESTination



.. autoclass:: RsPulseSeq.Implementations.Scenario.Destination.DestinationCls
	:members:
	:undoc-members:
	:noindex: