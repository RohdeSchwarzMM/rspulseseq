Instrument
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INSTrument:ADD
	single: INSTrument:CAPabilities
	single: INSTrument:CLEar
	single: INSTrument:COMMent
	single: INSTrument:CONNect
	single: INSTrument:COUNt
	single: INSTrument:DELete
	single: INSTrument:FIRMware
	single: INSTrument:LIST
	single: INSTrument:NAME
	single: INSTrument:ONLine
	single: INSTrument:PMOD
	single: INSTrument:PSEC
	single: INSTrument:RESource
	single: INSTrument:SELect
	single: INSTrument:SUPPorted
	single: INSTrument:TYPE
	single: INSTrument:VIRTual

.. code-block:: python

	INSTrument:ADD
	INSTrument:CAPabilities
	INSTrument:CLEar
	INSTrument:COMMent
	INSTrument:CONNect
	INSTrument:COUNt
	INSTrument:DELete
	INSTrument:FIRMware
	INSTrument:LIST
	INSTrument:NAME
	INSTrument:ONLine
	INSTrument:PMOD
	INSTrument:PSEC
	INSTrument:RESource
	INSTrument:SELect
	INSTrument:SUPPorted
	INSTrument:TYPE
	INSTrument:VIRTual



.. autoclass:: RsPulseSeq.Implementations.Instrument.InstrumentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.instrument.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Instrument_Adb.rst