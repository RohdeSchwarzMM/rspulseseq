Circular
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:CIRCular:DISTance
	single: ANTenna:MODel:CARRay:CIRCular:LATTice
	single: ANTenna:MODel:CARRay:CIRCular:RADius

.. code-block:: python

	ANTenna:MODel:CARRay:CIRCular:DISTance
	ANTenna:MODel:CARRay:CIRCular:LATTice
	ANTenna:MODel:CARRay:CIRCular:RADius



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Circular.CircularCls
	:members:
	:undoc-members:
	:noindex: