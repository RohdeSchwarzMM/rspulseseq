Sinc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ANTenna:MODel:SINC:RESolution

.. code-block:: python

	ANTenna:MODel:SINC:RESolution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Sinc.SincCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.sinc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_Sinc_HpBw.rst