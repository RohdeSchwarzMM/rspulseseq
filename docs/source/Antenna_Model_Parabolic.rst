Parabolic
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:PARabolic:DIAMeter
	single: ANTenna:MODel:PARabolic:RESolution

.. code-block:: python

	ANTenna:MODel:PARabolic:DIAMeter
	ANTenna:MODel:PARabolic:RESolution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Parabolic.ParabolicCls
	:members:
	:undoc-members:
	:noindex: