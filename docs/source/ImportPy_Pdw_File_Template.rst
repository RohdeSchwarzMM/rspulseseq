Template
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:FILE:TEMPlate:LOAD
	single: IMPort:PDW:FILE:TEMPlate:SAVE
	single: IMPort:PDW:FILE:TEMPlate

.. code-block:: python

	IMPort:PDW:FILE:TEMPlate:LOAD
	IMPort:PDW:FILE:TEMPlate:SAVE
	IMPort:PDW:FILE:TEMPlate



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.File.Template.TemplateCls
	:members:
	:undoc-members:
	:noindex: