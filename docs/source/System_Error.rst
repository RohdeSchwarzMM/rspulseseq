Error
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:ERRor:ALL
	single: SYSTem:ERRor:COUNt

.. code-block:: python

	SYSTem:ERRor:ALL
	SYSTem:ERRor:COUNt



.. autoclass:: RsPulseSeq.Implementations.System.Error.ErrorCls
	:members:
	:undoc-members:
	:noindex: