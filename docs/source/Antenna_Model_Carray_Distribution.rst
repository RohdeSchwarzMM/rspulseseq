Distribution
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:DISTribution:TYPE
	single: ANTenna:MODel:CARRay:DISTribution:X
	single: ANTenna:MODel:CARRay:DISTribution:Z
	single: ANTenna:MODel:CARRay:DISTribution

.. code-block:: python

	ANTenna:MODel:CARRay:DISTribution:TYPE
	ANTenna:MODel:CARRay:DISTribution:X
	ANTenna:MODel:CARRay:DISTribution:Z
	ANTenna:MODel:CARRay:DISTribution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Distribution.DistributionCls
	:members:
	:undoc-members:
	:noindex: