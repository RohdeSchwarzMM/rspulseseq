Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:GROup:ADD

.. code-block:: python

	SCENario:DF:GROup:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Group.Add.AddCls
	:members:
	:undoc-members:
	:noindex: