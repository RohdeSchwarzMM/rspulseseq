Marker
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MARKer:AUTO
	single: PULSe:MARKer:FALL
	single: PULSe:MARKer:GATE
	single: PULSe:MARKer:POST
	single: PULSe:MARKer:PRE
	single: PULSe:MARKer:RISE
	single: PULSe:MARKer:WIDTh

.. code-block:: python

	PULSe:MARKer:AUTO
	PULSe:MARKer:FALL
	PULSe:MARKer:GATE
	PULSe:MARKer:POST
	PULSe:MARKer:PRE
	PULSe:MARKer:RISE
	PULSe:MARKer:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Pulse.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex: