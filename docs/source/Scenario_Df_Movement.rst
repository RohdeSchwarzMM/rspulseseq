Movement
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:MOVement:ACCeleration
	single: SCENario:DF:MOVement:ALTitude
	single: SCENario:DF:MOVement:ANGLe
	single: SCENario:DF:MOVement:ATTitude
	single: SCENario:DF:MOVement:CLATitude
	single: SCENario:DF:MOVement:CLEar
	single: SCENario:DF:MOVement:CLONgitude
	single: SCENario:DF:MOVement:EAST
	single: SCENario:DF:MOVement:HEIGht
	single: SCENario:DF:MOVement:LATitude
	single: SCENario:DF:MOVement:LONGitude
	single: SCENario:DF:MOVement:NORTh
	single: SCENario:DF:MOVement:PITCh
	single: SCENario:DF:MOVement:RFRame
	single: SCENario:DF:MOVement:RMODe
	single: SCENario:DF:MOVement:ROLL
	single: SCENario:DF:MOVement:SMOothening
	single: SCENario:DF:MOVement:SPEed
	single: SCENario:DF:MOVement:SPINning
	single: SCENario:DF:MOVement:TYPE
	single: SCENario:DF:MOVement:VEHicle
	single: SCENario:DF:MOVement:WAYPoint
	single: SCENario:DF:MOVement:YAW

.. code-block:: python

	SCENario:DF:MOVement:ACCeleration
	SCENario:DF:MOVement:ALTitude
	SCENario:DF:MOVement:ANGLe
	SCENario:DF:MOVement:ATTitude
	SCENario:DF:MOVement:CLATitude
	SCENario:DF:MOVement:CLEar
	SCENario:DF:MOVement:CLONgitude
	SCENario:DF:MOVement:EAST
	SCENario:DF:MOVement:HEIGht
	SCENario:DF:MOVement:LATitude
	SCENario:DF:MOVement:LONGitude
	SCENario:DF:MOVement:NORTh
	SCENario:DF:MOVement:PITCh
	SCENario:DF:MOVement:RFRame
	SCENario:DF:MOVement:RMODe
	SCENario:DF:MOVement:ROLL
	SCENario:DF:MOVement:SMOothening
	SCENario:DF:MOVement:SPEed
	SCENario:DF:MOVement:SPINning
	SCENario:DF:MOVement:TYPE
	SCENario:DF:MOVement:VEHicle
	SCENario:DF:MOVement:WAYPoint
	SCENario:DF:MOVement:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Movement.MovementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.movement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Movement_ImportPy.rst
	Scenario_Df_Movement_Vfile.rst