Filler
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:FILLer:MODE
	single: SEQuence:ITEM:FILLer:SIGNal

.. code-block:: python

	SEQuence:ITEM:FILLer:MODE
	SEQuence:ITEM:FILLer:SIGNal



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Filler.FillerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sequence.item.filler.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sequence_Item_Filler_Time.rst