Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:EMITter:STATe:ADD

.. code-block:: python

	SCENario:DF:EMITter:STATe:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Emitter.State.Add.AddCls
	:members:
	:undoc-members:
	:noindex: