Helical
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:HELical:RETRace
	single: SCAN:HELical:ROTation
	single: SCAN:HELical:RPM
	single: SCAN:HELical:TURNs

.. code-block:: python

	SCAN:HELical:RETRace
	SCAN:HELical:ROTation
	SCAN:HELical:RPM
	SCAN:HELical:TURNs



.. autoclass:: RsPulseSeq.Implementations.Scan.Helical.HelicalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scan.helical.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scan_Helical_Elevation.rst