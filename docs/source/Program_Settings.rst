Settings
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Program.Settings.SettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.program.settings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Program_Settings_Accept.rst
	Program_Settings_Reject.rst