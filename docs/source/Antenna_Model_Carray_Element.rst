Element
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ANTenna:MODel:CARRay:ELEMent:COSine

.. code-block:: python

	ANTenna:MODel:CARRay:ELEMent:COSine



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Element.ElementCls
	:members:
	:undoc-members:
	:noindex: