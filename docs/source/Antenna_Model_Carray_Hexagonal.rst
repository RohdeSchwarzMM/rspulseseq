Hexagonal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:HEXagonal:DISTance
	single: ANTenna:MODel:CARRay:HEXagonal:N

.. code-block:: python

	ANTenna:MODel:CARRay:HEXagonal:DISTance
	ANTenna:MODel:CARRay:HEXagonal:N



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Hexagonal.HexagonalCls
	:members:
	:undoc-members:
	:noindex: