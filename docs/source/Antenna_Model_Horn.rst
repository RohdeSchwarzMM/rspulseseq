Horn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:HORN:LX
	single: ANTenna:MODel:HORN:LZ
	single: ANTenna:MODel:HORN:RESolution

.. code-block:: python

	ANTenna:MODel:HORN:LX
	ANTenna:MODel:HORN:LZ
	ANTenna:MODel:HORN:RESolution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Horn.HornCls
	:members:
	:undoc-members:
	:noindex: