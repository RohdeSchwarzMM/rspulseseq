Repository
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CACHe:REPository:CLEar
	single: SCENario:CACHe:REPository:VALid

.. code-block:: python

	SCENario:CACHe:REPository:CLEar
	SCENario:CACHe:REPository:VALid



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cache.Repository.RepositoryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cache.repository.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cache_Repository_Enable.rst