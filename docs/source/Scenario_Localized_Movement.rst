Movement
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:MOVement:ACCeleration
	single: SCENario:LOCalized:MOVement:ALTitude
	single: SCENario:LOCalized:MOVement:ANGLe
	single: SCENario:LOCalized:MOVement:ATTitude
	single: SCENario:LOCalized:MOVement:CLATitude
	single: SCENario:LOCalized:MOVement:CLEar
	single: SCENario:LOCalized:MOVement:CLONgitude
	single: SCENario:LOCalized:MOVement:EAST
	single: SCENario:LOCalized:MOVement:HEIGht
	single: SCENario:LOCalized:MOVement:LATitude
	single: SCENario:LOCalized:MOVement:LONGitude
	single: SCENario:LOCalized:MOVement:NORTh
	single: SCENario:LOCalized:MOVement:PITCh
	single: SCENario:LOCalized:MOVement:RFRame
	single: SCENario:LOCalized:MOVement:RMODe
	single: SCENario:LOCalized:MOVement:ROLL
	single: SCENario:LOCalized:MOVement:SMOothening
	single: SCENario:LOCalized:MOVement:SPEed
	single: SCENario:LOCalized:MOVement:SPINning
	single: SCENario:LOCalized:MOVement:TYPE
	single: SCENario:LOCalized:MOVement:VEHicle
	single: SCENario:LOCalized:MOVement:WAYPoint
	single: SCENario:LOCalized:MOVement:YAW

.. code-block:: python

	SCENario:LOCalized:MOVement:ACCeleration
	SCENario:LOCalized:MOVement:ALTitude
	SCENario:LOCalized:MOVement:ANGLe
	SCENario:LOCalized:MOVement:ATTitude
	SCENario:LOCalized:MOVement:CLATitude
	SCENario:LOCalized:MOVement:CLEar
	SCENario:LOCalized:MOVement:CLONgitude
	SCENario:LOCalized:MOVement:EAST
	SCENario:LOCalized:MOVement:HEIGht
	SCENario:LOCalized:MOVement:LATitude
	SCENario:LOCalized:MOVement:LONGitude
	SCENario:LOCalized:MOVement:NORTh
	SCENario:LOCalized:MOVement:PITCh
	SCENario:LOCalized:MOVement:RFRame
	SCENario:LOCalized:MOVement:RMODe
	SCENario:LOCalized:MOVement:ROLL
	SCENario:LOCalized:MOVement:SMOothening
	SCENario:LOCalized:MOVement:SPEed
	SCENario:LOCalized:MOVement:SPINning
	SCENario:LOCalized:MOVement:TYPE
	SCENario:LOCalized:MOVement:VEHicle
	SCENario:LOCalized:MOVement:WAYPoint
	SCENario:LOCalized:MOVement:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Movement.MovementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.movement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Movement_ImportPy.rst
	Scenario_Localized_Movement_Vfile.rst