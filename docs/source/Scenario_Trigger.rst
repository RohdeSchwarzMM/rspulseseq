Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:TRIGger

.. code-block:: python

	SCENario:TRIGger



.. autoclass:: RsPulseSeq.Implementations.Scenario.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: