Direction
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:RECeiver:DIRection:PITCh
	single: SCENario:LOCalized:RECeiver:DIRection:ROLL
	single: SCENario:LOCalized:RECeiver:DIRection:YAW

.. code-block:: python

	SCENario:LOCalized:RECeiver:DIRection:PITCh
	SCENario:LOCalized:RECeiver:DIRection:ROLL
	SCENario:LOCalized:RECeiver:DIRection:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Receiver.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: