Unmute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CPANel:UNMute

.. code-block:: python

	CPANel:UNMute



.. autoclass:: RsPulseSeq.Implementations.Cpanel.Unmute.UnmuteCls
	:members:
	:undoc-members:
	:noindex: