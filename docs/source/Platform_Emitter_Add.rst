Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PLATform:EMITter:ADD

.. code-block:: python

	PLATform:EMITter:ADD



.. autoclass:: RsPulseSeq.Implementations.Platform.Emitter.Add.AddCls
	:members:
	:undoc-members:
	:noindex: