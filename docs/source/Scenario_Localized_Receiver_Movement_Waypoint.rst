Waypoint
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:RECeiver:MOVement:WAYPoint:CLEar
	single: SCENario:LOCalized:RECeiver:MOVement:WAYPoint

.. code-block:: python

	SCENario:LOCalized:RECeiver:MOVement:WAYPoint:CLEar
	SCENario:LOCalized:RECeiver:MOVement:WAYPoint



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Receiver.Movement.Waypoint.WaypointCls
	:members:
	:undoc-members:
	:noindex: