Data
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:FREQuency
	single: IMPort:PDW:DATA:LEVel
	single: IMPort:PDW:DATA:MOP
	single: IMPort:PDW:DATA:OFFSet
	single: IMPort:PDW:DATA:PHASe
	single: IMPort:PDW:DATA:SEL
	single: IMPort:PDW:DATA:TOA
	single: IMPort:PDW:DATA:WIDTh

.. code-block:: python

	IMPort:PDW:DATA:FREQuency
	IMPort:PDW:DATA:LEVel
	IMPort:PDW:DATA:MOP
	IMPort:PDW:DATA:OFFSet
	IMPort:PDW:DATA:PHASe
	IMPort:PDW:DATA:SEL
	IMPort:PDW:DATA:TOA
	IMPort:PDW:DATA:WIDTh



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.DataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.importPy.pdw.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	ImportPy_Pdw_Data_Am.rst
	ImportPy_Pdw_Data_Ask.rst
	ImportPy_Pdw_Data_Cph.rst
	ImportPy_Pdw_Data_Fm.rst
	ImportPy_Pdw_Data_Fsk.rst
	ImportPy_Pdw_Data_Lfm.rst
	ImportPy_Pdw_Data_NlFm.rst
	ImportPy_Pdw_Data_PlFm.rst
	ImportPy_Pdw_Data_Psk.rst