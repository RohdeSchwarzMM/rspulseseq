Sequence
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:CATalog
	single: SEQuence:COMMent
	single: SEQuence:CREate
	single: SEQuence:NAME
	single: SEQuence:REMove
	single: SEQuence:SELect
	single: SEQuence:TYPE

.. code-block:: python

	SEQuence:CATalog
	SEQuence:COMMent
	SEQuence:CREate
	SEQuence:NAME
	SEQuence:REMove
	SEQuence:SELect
	SEQuence:TYPE



.. autoclass:: RsPulseSeq.Implementations.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sequence_Item.rst
	Sequence_Phase.rst
	Sequence_Time.rst