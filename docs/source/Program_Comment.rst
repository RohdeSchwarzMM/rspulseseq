Comment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:COMMent:ENABle

.. code-block:: python

	PROGram:COMMent:ENABle



.. autoclass:: RsPulseSeq.Implementations.Program.Comment.CommentCls
	:members:
	:undoc-members:
	:noindex: