Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:EMITter:MODE:BEAM
	single: SCENario:EMITter:MODE

.. code-block:: python

	SCENario:EMITter:MODE:BEAM
	SCENario:EMITter:MODE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Emitter.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: