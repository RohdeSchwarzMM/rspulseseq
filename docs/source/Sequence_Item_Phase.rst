Phase
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SEQuence:ITEM:PHASe:OFFSet

.. code-block:: python

	SEQuence:ITEM:PHASe:OFFSet



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex: