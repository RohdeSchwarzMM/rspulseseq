Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:CEMit:MCHG:ADD

.. code-block:: python

	SCENario:CEMit:MCHG:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Mchg.Add.AddCls
	:members:
	:undoc-members:
	:noindex: