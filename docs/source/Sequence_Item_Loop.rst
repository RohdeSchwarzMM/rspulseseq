Loop
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:LOOP:TYPE
	single: SEQuence:ITEM:LOOP:VARiable

.. code-block:: python

	SEQuence:ITEM:LOOP:TYPE
	SEQuence:ITEM:LOOP:VARiable



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Loop.LoopCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sequence.item.loop.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sequence_Item_Loop_Count.rst