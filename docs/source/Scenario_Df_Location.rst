Location
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:LOCation:ALTitude
	single: SCENario:DF:LOCation:AZIMuth
	single: SCENario:DF:LOCation:EAST
	single: SCENario:DF:LOCation:ELEVation
	single: SCENario:DF:LOCation:HEIGht
	single: SCENario:DF:LOCation:LATitude
	single: SCENario:DF:LOCation:LONGitude
	single: SCENario:DF:LOCation:NORTh
	single: SCENario:DF:LOCation:PMODe

.. code-block:: python

	SCENario:DF:LOCation:ALTitude
	SCENario:DF:LOCation:AZIMuth
	SCENario:DF:LOCation:EAST
	SCENario:DF:LOCation:ELEVation
	SCENario:DF:LOCation:HEIGht
	SCENario:DF:LOCation:LATitude
	SCENario:DF:LOCation:LONGitude
	SCENario:DF:LOCation:NORTh
	SCENario:DF:LOCation:PMODe



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Location.LocationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.location.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Location_Pstep.rst
	Scenario_Df_Location_Rec.rst
	Scenario_Df_Location_Waypoint.rst