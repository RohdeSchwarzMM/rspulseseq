Marker
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:OUTPut:MARKer:ENABle
	single: SCENario:OUTPut:MARKer:FLAGs

.. code-block:: python

	SCENario:OUTPut:MARKer:ENABle
	SCENario:OUTPut:MARKer:FLAGs



.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.output.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Output_Marker_Scenario.rst