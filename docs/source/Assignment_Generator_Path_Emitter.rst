Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:GENerator:PATH:EMITter:CLEar
	single: ASSignment:GENerator:PATH:EMITter:DELete
	single: ASSignment:GENerator:PATH:EMITter:LIST
	single: ASSignment:GENerator:PATH:EMITter:SELect

.. code-block:: python

	ASSignment:GENerator:PATH:EMITter:CLEar
	ASSignment:GENerator:PATH:EMITter:DELete
	ASSignment:GENerator:PATH:EMITter:LIST
	ASSignment:GENerator:PATH:EMITter:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Generator.Path.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.generator.path.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Generator_Path_Emitter_Add.rst