Clock
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:OUTPut:CLOCk:MODE
	single: SCENario:OUTPut:CLOCk:USER

.. code-block:: python

	SCENario:OUTPut:CLOCk:MODE
	SCENario:OUTPut:CLOCk:USER



.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.Clock.ClockCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.output.clock.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Output_Clock_Auto.rst