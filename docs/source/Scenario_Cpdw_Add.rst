Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:CPDW:ADD

.. code-block:: python

	SCENario:CPDW:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cpdw.Add.AddCls
	:members:
	:undoc-members:
	:noindex: