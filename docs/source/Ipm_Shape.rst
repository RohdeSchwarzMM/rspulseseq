Shape
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:SHAPe:BASE
	single: IPM:SHAPe:COUNt
	single: IPM:SHAPe:INTerpol
	single: IPM:SHAPe:PERiod

.. code-block:: python

	IPM:SHAPe:BASE
	IPM:SHAPe:COUNt
	IPM:SHAPe:INTerpol
	IPM:SHAPe:PERiod



.. autoclass:: RsPulseSeq.Implementations.Ipm.Shape.ShapeCls
	:members:
	:undoc-members:
	:noindex: