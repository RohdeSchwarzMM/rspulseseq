Csequence
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CSEQuence:ALIas
	single: SCENario:CSEQuence:CLEar
	single: SCENario:CSEQuence:CURRent
	single: SCENario:CSEQuence:DELete
	single: SCENario:CSEQuence:SELect
	single: SCENario:CSEQuence:VARiable
	single: SCENario:CSEQuence

.. code-block:: python

	SCENario:CSEQuence:ALIas
	SCENario:CSEQuence:CLEar
	SCENario:CSEQuence:CURRent
	SCENario:CSEQuence:DELete
	SCENario:CSEQuence:SELect
	SCENario:CSEQuence:VARiable
	SCENario:CSEQuence



.. autoclass:: RsPulseSeq.Implementations.Scenario.Csequence.CsequenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.csequence.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Csequence_Add.rst