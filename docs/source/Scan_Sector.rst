Sector
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:SECTor:FLYBack
	single: SCAN:SECTor:NELevation
	single: SCAN:SECTor:NODDing
	single: SCAN:SECTor:NRATe
	single: SCAN:SECTor:PALMer
	single: SCAN:SECTor:PRATe
	single: SCAN:SECTor:PSQuint
	single: SCAN:SECTor:RATE
	single: SCAN:SECTor:UNIDirection
	single: SCAN:SECTor:WIDTh

.. code-block:: python

	SCAN:SECTor:FLYBack
	SCAN:SECTor:NELevation
	SCAN:SECTor:NODDing
	SCAN:SECTor:NRATe
	SCAN:SECTor:PALMer
	SCAN:SECTor:PRATe
	SCAN:SECTor:PSQuint
	SCAN:SECTor:RATE
	SCAN:SECTor:UNIDirection
	SCAN:SECTor:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Scan.Sector.SectorCls
	:members:
	:undoc-members:
	:noindex: