Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:EMITter:MODE:BEAM
	single: SCENario:CEMit:EMITter:MODE:TRACkrec
	single: SCENario:CEMit:EMITter:MODE

.. code-block:: python

	SCENario:CEMit:EMITter:MODE:BEAM
	SCENario:CEMit:EMITter:MODE:TRACkrec
	SCENario:CEMit:EMITter:MODE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Emitter.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: