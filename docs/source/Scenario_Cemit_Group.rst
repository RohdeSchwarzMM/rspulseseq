Group
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:GROup:ALIas
	single: SCENario:CEMit:GROup:CATalog
	single: SCENario:CEMit:GROup:CLEar
	single: SCENario:CEMit:GROup:COUNt
	single: SCENario:CEMit:GROup:DELete
	single: SCENario:CEMit:GROup:SELect
	single: SCENario:CEMit:GROup

.. code-block:: python

	SCENario:CEMit:GROup:ALIas
	SCENario:CEMit:GROup:CATalog
	SCENario:CEMit:GROup:CLEar
	SCENario:CEMit:GROup:COUNt
	SCENario:CEMit:GROup:DELete
	SCENario:CEMit:GROup:SELect
	SCENario:CEMit:GROup



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Group.GroupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cemit.group.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cemit_Group_Add.rst