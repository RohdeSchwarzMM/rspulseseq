Piecewise
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:PIECewise:CLEar
	single: PULSe:MOP:PIECewise:COUNt
	single: PULSe:MOP:PIECewise:DELete
	single: PULSe:MOP:PIECewise:DURation
	single: PULSe:MOP:PIECewise:INSert
	single: PULSe:MOP:PIECewise:OFFSet
	single: PULSe:MOP:PIECewise:RATE
	single: PULSe:MOP:PIECewise:SELect

.. code-block:: python

	PULSe:MOP:PIECewise:CLEar
	PULSe:MOP:PIECewise:COUNt
	PULSe:MOP:PIECewise:DELete
	PULSe:MOP:PIECewise:DURation
	PULSe:MOP:PIECewise:INSert
	PULSe:MOP:PIECewise:OFFSet
	PULSe:MOP:PIECewise:RATE
	PULSe:MOP:PIECewise:SELect



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Piecewise.PiecewiseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.piecewise.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Piecewise_Add.rst