Dialog
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Dialog.DialogCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.dialog.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Dialog_IpmPlot.rst