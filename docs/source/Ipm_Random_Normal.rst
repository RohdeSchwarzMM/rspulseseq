Normal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:RANDom:NORMal:LIMit
	single: IPM:RANDom:NORMal:MEAN
	single: IPM:RANDom:NORMal:STD

.. code-block:: python

	IPM:RANDom:NORMal:LIMit
	IPM:RANDom:NORMal:MEAN
	IPM:RANDom:NORMal:STD



.. autoclass:: RsPulseSeq.Implementations.Ipm.Random.Normal.NormalCls
	:members:
	:undoc-members:
	:noindex: