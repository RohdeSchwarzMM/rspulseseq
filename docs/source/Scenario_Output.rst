Output
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:OUTPut:CLIPping
	single: SCENario:OUTPut:FORMat
	single: SCENario:OUTPut:FREQuency
	single: SCENario:OUTPut:LEVel
	single: SCENario:OUTPut:MTMode
	single: SCENario:OUTPut:MTTHreads
	single: SCENario:OUTPut:MULTithread
	single: SCENario:OUTPut:PATH
	single: SCENario:OUTPut:RUNMode
	single: SCENario:OUTPut:TARGet
	single: SCENario:OUTPut:THReshold

.. code-block:: python

	SCENario:OUTPut:CLIPping
	SCENario:OUTPut:FORMat
	SCENario:OUTPut:FREQuency
	SCENario:OUTPut:LEVel
	SCENario:OUTPut:MTMode
	SCENario:OUTPut:MTTHreads
	SCENario:OUTPut:MULTithread
	SCENario:OUTPut:PATH
	SCENario:OUTPut:RUNMode
	SCENario:OUTPut:TARGet
	SCENario:OUTPut:THReshold



.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.OutputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.output.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Output_Arb.rst
	Scenario_Output_Clock.rst
	Scenario_Output_Duration.rst
	Scenario_Output_Marker.rst
	Scenario_Output_Recall.rst
	Scenario_Output_Reset.rst
	Scenario_Output_Supress.rst