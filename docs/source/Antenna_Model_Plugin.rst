Plugin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ANTenna:MODel:PLUGin:NAME

.. code-block:: python

	ANTenna:MODel:PLUGin:NAME



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Plugin.PluginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.plugin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_Plugin_Variable.rst