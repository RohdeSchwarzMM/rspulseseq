Offset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: EMITter:MODE:BEAM:OFFSet:AZIMuth
	single: EMITter:MODE:BEAM:OFFSet:ELEVation
	single: EMITter:MODE:BEAM:OFFSet:FREQuency

.. code-block:: python

	EMITter:MODE:BEAM:OFFSet:AZIMuth
	EMITter:MODE:BEAM:OFFSet:ELEVation
	EMITter:MODE:BEAM:OFFSet:FREQuency



.. autoclass:: RsPulseSeq.Implementations.Emitter.Mode.Beam.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: