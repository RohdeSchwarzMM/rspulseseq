Program
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:MODE

.. code-block:: python

	PROGram:MODE



.. autoclass:: RsPulseSeq.Implementations.Program.ProgramCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.program.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Program_Adjustments.rst
	Program_ClassPy.rst
	Program_Comment.rst
	Program_Gpu.rst
	Program_Hide.rst
	Program_Path.rst
	Program_RamBuff.rst
	Program_Scenario.rst
	Program_Settings.rst
	Program_Show.rst
	Program_Startup.rst
	Program_StorageLoc.rst
	Program_Toolbar.rst
	Program_Transfer.rst
	Program_Tutorials.rst