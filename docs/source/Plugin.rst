Plugin
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PLUGin:CATalog
	single: PLUGin:COMMent
	single: PLUGin:CREate
	single: PLUGin:LOAD
	single: PLUGin:NAME
	single: PLUGin:REMove
	single: PLUGin:SELect

.. code-block:: python

	PLUGin:CATalog
	PLUGin:COMMent
	PLUGin:CREate
	PLUGin:LOAD
	PLUGin:NAME
	PLUGin:REMove
	PLUGin:SELect



.. autoclass:: RsPulseSeq.Implementations.Plugin.PluginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.plugin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Plugin_Module.rst