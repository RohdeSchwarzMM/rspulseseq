Group
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CPDW:GROup:ALIas
	single: SCENario:CPDW:GROup:CATalog
	single: SCENario:CPDW:GROup:CLEar
	single: SCENario:CPDW:GROup:COUNt
	single: SCENario:CPDW:GROup:DELete
	single: SCENario:CPDW:GROup:SELect
	single: SCENario:CPDW:GROup

.. code-block:: python

	SCENario:CPDW:GROup:ALIas
	SCENario:CPDW:GROup:CATalog
	SCENario:CPDW:GROup:CLEar
	SCENario:CPDW:GROup:COUNt
	SCENario:CPDW:GROup:DELete
	SCENario:CPDW:GROup:SELect
	SCENario:CPDW:GROup



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cpdw.Group.GroupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cpdw.group.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cpdw_Group_Add.rst