Rec
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:LOCation:REC:PMODe

.. code-block:: python

	SCENario:DF:LOCation:REC:PMODe



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Location.Rec.RecCls
	:members:
	:undoc-members:
	:noindex: