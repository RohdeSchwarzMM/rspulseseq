Target
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:IPM:TARGet:PARameter
	single: SEQuence:ITEM:IPM:TARGet:TYPE
	single: SEQuence:ITEM:IPM:TARGet:VARiable

.. code-block:: python

	SEQuence:ITEM:IPM:TARGet:PARameter
	SEQuence:ITEM:IPM:TARGet:TYPE
	SEQuence:ITEM:IPM:TARGet:VARiable



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Ipm.Target.TargetCls
	:members:
	:undoc-members:
	:noindex: