Item
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:ENVelope:DATA:ITEM:COUNt
	single: PULSe:ENVelope:DATA:ITEM:DELete
	single: PULSe:ENVelope:DATA:ITEM:SELect
	single: PULSe:ENVelope:DATA:ITEM:VALue

.. code-block:: python

	PULSe:ENVelope:DATA:ITEM:COUNt
	PULSe:ENVelope:DATA:ITEM:DELete
	PULSe:ENVelope:DATA:ITEM:SELect
	PULSe:ENVelope:DATA:ITEM:VALue



.. autoclass:: RsPulseSeq.Implementations.Pulse.Envelope.Data.Item.ItemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.envelope.data.item.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Envelope_Data_Item_Add.rst