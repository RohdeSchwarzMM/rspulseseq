Cardoid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARDoid:EXPonent
	single: ANTenna:MODel:CARDoid:RESolution

.. code-block:: python

	ANTenna:MODel:CARDoid:EXPonent
	ANTenna:MODel:CARDoid:RESolution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Cardoid.CardoidCls
	:members:
	:undoc-members:
	:noindex: