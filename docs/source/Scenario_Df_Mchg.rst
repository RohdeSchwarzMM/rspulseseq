Mchg
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:MCHG:CLEar
	single: SCENario:DF:MCHG:COUNt
	single: SCENario:DF:MCHG:DELete
	single: SCENario:DF:MCHG:SELect
	single: SCENario:DF:MCHG:STARt
	single: SCENario:DF:MCHG:STATe
	single: SCENario:DF:MCHG:STOP

.. code-block:: python

	SCENario:DF:MCHG:CLEar
	SCENario:DF:MCHG:COUNt
	SCENario:DF:MCHG:DELete
	SCENario:DF:MCHG:SELect
	SCENario:DF:MCHG:STARt
	SCENario:DF:MCHG:STATe
	SCENario:DF:MCHG:STOP



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Mchg.MchgCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.mchg.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Mchg_Add.rst