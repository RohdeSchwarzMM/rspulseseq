Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:CEMit:ADD

.. code-block:: python

	SCENario:CEMit:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Add.AddCls
	:members:
	:undoc-members:
	:noindex: