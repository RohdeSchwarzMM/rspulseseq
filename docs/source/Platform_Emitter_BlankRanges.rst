BlankRanges
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PLATform:EMITter:BLANkranges:CLEar
	single: PLATform:EMITter:BLANkranges:COUNt
	single: PLATform:EMITter:BLANkranges:DELete
	single: PLATform:EMITter:BLANkranges:SELect
	single: PLATform:EMITter:BLANkranges:STARt
	single: PLATform:EMITter:BLANkranges:STOP

.. code-block:: python

	PLATform:EMITter:BLANkranges:CLEar
	PLATform:EMITter:BLANkranges:COUNt
	PLATform:EMITter:BLANkranges:DELete
	PLATform:EMITter:BLANkranges:SELect
	PLATform:EMITter:BLANkranges:STARt
	PLATform:EMITter:BLANkranges:STOP



.. autoclass:: RsPulseSeq.Implementations.Platform.Emitter.BlankRanges.BlankRangesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.platform.emitter.blankRanges.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Platform_Emitter_BlankRanges_Add.rst