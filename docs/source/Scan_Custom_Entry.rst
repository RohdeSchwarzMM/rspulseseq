Entry
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:CUSTom:ENTRy:AZIMuth
	single: SCAN:CUSTom:ENTRy:CLEar
	single: SCAN:CUSTom:ENTRy:COUNt
	single: SCAN:CUSTom:ENTRy:DELete
	single: SCAN:CUSTom:ENTRy:DWELl
	single: SCAN:CUSTom:ENTRy:ELEVation
	single: SCAN:CUSTom:ENTRy:INSert
	single: SCAN:CUSTom:ENTRy:JUMPtype
	single: SCAN:CUSTom:ENTRy:SELect
	single: SCAN:CUSTom:ENTRy:TRANstime

.. code-block:: python

	SCAN:CUSTom:ENTRy:AZIMuth
	SCAN:CUSTom:ENTRy:CLEar
	SCAN:CUSTom:ENTRy:COUNt
	SCAN:CUSTom:ENTRy:DELete
	SCAN:CUSTom:ENTRy:DWELl
	SCAN:CUSTom:ENTRy:ELEVation
	SCAN:CUSTom:ENTRy:INSert
	SCAN:CUSTom:ENTRy:JUMPtype
	SCAN:CUSTom:ENTRy:SELect
	SCAN:CUSTom:ENTRy:TRANstime



.. autoclass:: RsPulseSeq.Implementations.Scan.Custom.Entry.EntryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scan.custom.entry.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scan_Custom_Entry_Add.rst