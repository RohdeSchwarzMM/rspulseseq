Ipm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:IPM:COUNt
	single: SEQuence:ITEM:IPM:DELete
	single: SEQuence:ITEM:IPM:EQUation
	single: SEQuence:ITEM:IPM:MODE
	single: SEQuence:ITEM:IPM:RESTart
	single: SEQuence:ITEM:IPM:SELect

.. code-block:: python

	SEQuence:ITEM:IPM:COUNt
	SEQuence:ITEM:IPM:DELete
	SEQuence:ITEM:IPM:EQUation
	SEQuence:ITEM:IPM:MODE
	SEQuence:ITEM:IPM:RESTart
	SEQuence:ITEM:IPM:SELect



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Ipm.IpmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sequence.item.ipm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sequence_Item_Ipm_Add.rst
	Sequence_Item_Ipm_Random.rst
	Sequence_Item_Ipm_Source.rst
	Sequence_Item_Ipm_Target.rst