Synchronize
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:SYNChronize:ENABle

.. code-block:: python

	SCENario:DF:SYNChronize:ENABle



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Synchronize.SynchronizeCls
	:members:
	:undoc-members:
	:noindex: