Noise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:NOISe:BWIDth

.. code-block:: python

	PULSe:MOP:NOISe:BWIDth



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Noise.NoiseCls
	:members:
	:undoc-members:
	:noindex: