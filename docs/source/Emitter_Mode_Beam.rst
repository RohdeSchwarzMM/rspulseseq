Beam
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: EMITter:MODE:BEAM:CLEar
	single: EMITter:MODE:BEAM:COUNt
	single: EMITter:MODE:BEAM:DELete
	single: EMITter:MODE:BEAM:NAME
	single: EMITter:MODE:BEAM:SELect
	single: EMITter:MODE:BEAM:SEQuence
	single: EMITter:MODE:BEAM:STATe

.. code-block:: python

	EMITter:MODE:BEAM:CLEar
	EMITter:MODE:BEAM:COUNt
	EMITter:MODE:BEAM:DELete
	EMITter:MODE:BEAM:NAME
	EMITter:MODE:BEAM:SELect
	EMITter:MODE:BEAM:SEQuence
	EMITter:MODE:BEAM:STATe



.. autoclass:: RsPulseSeq.Implementations.Emitter.Mode.Beam.BeamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.emitter.mode.beam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Emitter_Mode_Beam_Add.rst
	Emitter_Mode_Beam_Offset.rst