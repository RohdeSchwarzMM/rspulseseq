ImportPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCAN:CUSTom:IMPort:FILE

.. code-block:: python

	SCAN:CUSTom:IMPort:FILE



.. autoclass:: RsPulseSeq.Implementations.Scan.Custom.ImportPy.ImportPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scan.custom.importPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scan_Custom_ImportPy_Exec.rst