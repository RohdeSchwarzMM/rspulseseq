Path
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:DESTination:PATH:LIST
	single: ASSignment:DESTination:PATH:SELect

.. code-block:: python

	ASSignment:DESTination:PATH:LIST
	ASSignment:DESTination:PATH:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Destination.Path.PathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.destination.path.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Destination_Path_Antenna.rst
	Assignment_Destination_Path_Emitter.rst