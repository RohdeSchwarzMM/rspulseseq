Locpl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SETup:LOCPl:ENABle

.. code-block:: python

	SETup:LOCPl:ENABle



.. autoclass:: RsPulseSeq.Implementations.Setup.Locpl.LocplCls
	:members:
	:undoc-members:
	:noindex: