RamBuff
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:RAMBuff:SIZE

.. code-block:: python

	PROGram:RAMBuff:SIZE



.. autoclass:: RsPulseSeq.Implementations.Program.RamBuff.RamBuffCls
	:members:
	:undoc-members:
	:noindex: