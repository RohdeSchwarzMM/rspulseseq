Generator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: GENerator:TYPE

.. code-block:: python

	GENerator:TYPE



.. autoclass:: RsPulseSeq.Implementations.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex: