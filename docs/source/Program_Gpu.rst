Gpu
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:GPU:ENABle

.. code-block:: python

	PROGram:GPU:ENABle



.. autoclass:: RsPulseSeq.Implementations.Program.Gpu.GpuCls
	:members:
	:undoc-members:
	:noindex: