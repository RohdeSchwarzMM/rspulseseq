Df
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:ALIas
	single: SCENario:DF:CLEar
	single: SCENario:DF:CURRent
	single: SCENario:DF:DELete
	single: SCENario:DF:DISTance
	single: SCENario:DF:ENABle
	single: SCENario:DF:FREQuency
	single: SCENario:DF:LDELay
	single: SCENario:DF:LEVel
	single: SCENario:DF:PRIority
	single: SCENario:DF:SELect
	single: SCENario:DF:SEQuence
	single: SCENario:DF:THReshold
	single: SCENario:DF:TYPE

.. code-block:: python

	SCENario:DF:ALIas
	SCENario:DF:CLEar
	SCENario:DF:CURRent
	SCENario:DF:DELete
	SCENario:DF:DISTance
	SCENario:DF:ENABle
	SCENario:DF:FREQuency
	SCENario:DF:LDELay
	SCENario:DF:LEVel
	SCENario:DF:PRIority
	SCENario:DF:SELect
	SCENario:DF:SEQuence
	SCENario:DF:THReshold
	SCENario:DF:TYPE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.DfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Add.rst
	Scenario_Df_Direction.rst
	Scenario_Df_Emitter.rst
	Scenario_Df_Group.rst
	Scenario_Df_Interleaving.rst
	Scenario_Df_Location.rst
	Scenario_Df_Maps.rst
	Scenario_Df_Marker.rst
	Scenario_Df_Mchg.rst
	Scenario_Df_Movement.rst
	Scenario_Df_Receiver.rst
	Scenario_Df_Subitem.rst
	Scenario_Df_Synchronize.rst
	Scenario_Df_Waveform.rst