Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCAN:CUSTom:ENTRy:ADD

.. code-block:: python

	SCAN:CUSTom:ENTRy:ADD



.. autoclass:: RsPulseSeq.Implementations.Scan.Custom.Entry.Add.AddCls
	:members:
	:undoc-members:
	:noindex: