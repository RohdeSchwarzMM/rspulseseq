Ftp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PROGram:TRANsfer:FTP:BLOCksize
	single: PROGram:TRANsfer:FTP:ENABle
	single: PROGram:TRANsfer:FTP:PASSwd
	single: PROGram:TRANsfer:FTP:UNAMe

.. code-block:: python

	PROGram:TRANsfer:FTP:BLOCksize
	PROGram:TRANsfer:FTP:ENABle
	PROGram:TRANsfer:FTP:PASSwd
	PROGram:TRANsfer:FTP:UNAMe



.. autoclass:: RsPulseSeq.Implementations.Program.Transfer.Ftp.FtpCls
	:members:
	:undoc-members:
	:noindex: