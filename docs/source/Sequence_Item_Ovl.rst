Ovl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:OVL:VARiable
	single: SEQuence:ITEM:OVL:WTIMe

.. code-block:: python

	SEQuence:ITEM:OVL:VARiable
	SEQuence:ITEM:OVL:WTIMe



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Ovl.OvlCls
	:members:
	:undoc-members:
	:noindex: