Direction
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:DIRection:PITCh
	single: SCENario:LOCalized:DIRection:ROLL
	single: SCENario:LOCalized:DIRection:TRACk
	single: SCENario:LOCalized:DIRection:YAW

.. code-block:: python

	SCENario:LOCalized:DIRection:PITCh
	SCENario:LOCalized:DIRection:ROLL
	SCENario:LOCalized:DIRection:TRACk
	SCENario:LOCalized:DIRection:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: