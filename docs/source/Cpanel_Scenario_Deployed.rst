Deployed
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CPANel:SCENario:DEPLoyed:NAME
	single: CPANel:SCENario:DEPLoyed:TIME

.. code-block:: python

	CPANel:SCENario:DEPLoyed:NAME
	CPANel:SCENario:DEPLoyed:TIME



.. autoclass:: RsPulseSeq.Implementations.Cpanel.Scenario.Deployed.DeployedCls
	:members:
	:undoc-members:
	:noindex: