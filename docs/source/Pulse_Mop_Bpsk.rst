Bpsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:BPSK:PHASe
	single: PULSe:MOP:BPSK:TTIMe
	single: PULSe:MOP:BPSK:TTYPe
	single: PULSe:MOP:BPSK:TYPE

.. code-block:: python

	PULSe:MOP:BPSK:PHASe
	PULSe:MOP:BPSK:TTIMe
	PULSe:MOP:BPSK:TTYPe
	PULSe:MOP:BPSK:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Bpsk.BpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.bpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Bpsk_SymbolRate.rst