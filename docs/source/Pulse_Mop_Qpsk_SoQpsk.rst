SoQpsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:QPSK:SOQPsk:IRIG

.. code-block:: python

	PULSe:MOP:QPSK:SOQPsk:IRIG



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Qpsk.SoQpsk.SoQpskCls
	:members:
	:undoc-members:
	:noindex: