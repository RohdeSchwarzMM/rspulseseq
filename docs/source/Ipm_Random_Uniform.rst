Uniform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:RANDom:UNIForm:MAXimum
	single: IPM:RANDom:UNIForm:MINimum
	single: IPM:RANDom:UNIForm:STEP

.. code-block:: python

	IPM:RANDom:UNIForm:MAXimum
	IPM:RANDom:UNIForm:MINimum
	IPM:RANDom:UNIForm:STEP



.. autoclass:: RsPulseSeq.Implementations.Ipm.Random.Uniform.UniformCls
	:members:
	:undoc-members:
	:noindex: