Show
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ARBComposer:SHOW

.. code-block:: python

	ARBComposer:SHOW



.. autoclass:: RsPulseSeq.Implementations.ArbComposer.Show.ShowCls
	:members:
	:undoc-members:
	:noindex: