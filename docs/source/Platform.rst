Platform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PLATform:ID
	single: PLATform:CATalog
	single: PLATform:COMMent
	single: PLATform:CREate
	single: PLATform:NAME
	single: PLATform:REMove
	single: PLATform:SELect

.. code-block:: python

	PLATform:ID
	PLATform:CATalog
	PLATform:COMMent
	PLATform:CREate
	PLATform:NAME
	PLATform:REMove
	PLATform:SELect



.. autoclass:: RsPulseSeq.Implementations.Platform.PlatformCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.platform.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Platform_Emitter.rst