Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ASSignment:DESTination:PATH:ANTenna:ADD

.. code-block:: python

	ASSignment:DESTination:PATH:ANTenna:ADD



.. autoclass:: RsPulseSeq.Implementations.Assignment.Destination.Path.Antenna.Add.AddCls
	:members:
	:undoc-members:
	:noindex: