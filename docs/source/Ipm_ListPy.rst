ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:LIST:BASE
	single: IPM:LIST:CLEar
	single: IPM:LIST:LOAD
	single: IPM:LIST:SAVE

.. code-block:: python

	IPM:LIST:BASE
	IPM:LIST:CLEar
	IPM:LIST:LOAD
	IPM:LIST:SAVE



.. autoclass:: RsPulseSeq.Implementations.Ipm.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ipm.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ipm_ListPy_Firing.rst
	Ipm_ListPy_Item.rst