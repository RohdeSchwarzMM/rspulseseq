Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SEQuence:ITEM:ADD

.. code-block:: python

	SEQuence:ITEM:ADD



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Add.AddCls
	:members:
	:undoc-members:
	:noindex: