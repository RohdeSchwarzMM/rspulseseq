Envelope
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:ENVelope:EQUation
	single: PULSe:ENVelope:MODE

.. code-block:: python

	PULSe:ENVelope:EQUation
	PULSe:ENVelope:MODE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Envelope.EnvelopeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.envelope.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Envelope_Data.rst