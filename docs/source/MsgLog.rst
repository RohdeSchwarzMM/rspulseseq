MsgLog
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: MSGLog:ERRor
	single: MSGLog:POPup

.. code-block:: python

	MSGLog:ERRor
	MSGLog:POPup



.. autoclass:: RsPulseSeq.Implementations.MsgLog.MsgLogCls
	:members:
	:undoc-members:
	:noindex: