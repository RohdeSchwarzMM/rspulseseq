Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:FMSTep:ADD

.. code-block:: python

	PULSe:MOP:FMSTep:ADD



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.FmStep.Add.AddCls
	:members:
	:undoc-members:
	:noindex: