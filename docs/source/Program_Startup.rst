Startup
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Program.Startup.StartupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.program.startup.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Program_Startup_Load.rst
	Program_Startup_Wizard.rst