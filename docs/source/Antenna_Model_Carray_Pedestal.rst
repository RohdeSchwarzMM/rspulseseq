Pedestal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:PEDestal:X
	single: ANTenna:MODel:CARRay:PEDestal:Z
	single: ANTenna:MODel:CARRay:PEDestal

.. code-block:: python

	ANTenna:MODel:CARRay:PEDestal:X
	ANTenna:MODel:CARRay:PEDestal:Z
	ANTenna:MODel:CARRay:PEDestal



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Pedestal.PedestalCls
	:members:
	:undoc-members:
	:noindex: