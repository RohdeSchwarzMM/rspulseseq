Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:DATA:CODing

.. code-block:: python

	PULSe:MOP:DATA:CODing



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Data.DataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Data_Dsrc.rst