Script
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCRipt:ADD

.. code-block:: python

	SCRipt:ADD



.. autoclass:: RsPulseSeq.Implementations.Script.ScriptCls
	:members:
	:undoc-members:
	:noindex: