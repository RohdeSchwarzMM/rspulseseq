Variable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:PLUGin:VARiable:CATalog
	single: ANTenna:MODel:PLUGin:VARiable:SELect
	single: ANTenna:MODel:PLUGin:VARiable:VALue

.. code-block:: python

	ANTenna:MODel:PLUGin:VARiable:CATalog
	ANTenna:MODel:PLUGin:VARiable:SELect
	ANTenna:MODel:PLUGin:VARiable:VALue



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Plugin.Variable.VariableCls
	:members:
	:undoc-members:
	:noindex: