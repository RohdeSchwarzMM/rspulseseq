Marker
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:MARKer:ALL
	single: SEQuence:ITEM:MARKer:FIRSt
	single: SEQuence:ITEM:MARKer:LAST

.. code-block:: python

	SEQuence:ITEM:MARKer:ALL
	SEQuence:ITEM:MARKer:FIRSt
	SEQuence:ITEM:MARKer:LAST



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sequence.item.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sequence_Item_Marker_Condition.rst