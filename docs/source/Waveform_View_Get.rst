Get
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: WAVeform:VIEW:GET:DURation

.. code-block:: python

	WAVeform:VIEW:GET:DURation



.. autoclass:: RsPulseSeq.Implementations.Waveform.View.Get.GetCls
	:members:
	:undoc-members:
	:noindex: