Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: EMITter:CATalog
	single: EMITter:COMMent
	single: EMITter:CREate
	single: EMITter:EIRP
	single: EMITter:FREQuency
	single: EMITter:NAME
	single: EMITter:REMove
	single: EMITter:SELect

.. code-block:: python

	EMITter:CATalog
	EMITter:COMMent
	EMITter:CREate
	EMITter:EIRP
	EMITter:FREQuency
	EMITter:NAME
	EMITter:REMove
	EMITter:SELect



.. autoclass:: RsPulseSeq.Implementations.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Emitter_Mode.rst