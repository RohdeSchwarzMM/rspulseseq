ImportPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:RECeiver:MOVement:IMPort

.. code-block:: python

	SCENario:LOCalized:RECeiver:MOVement:IMPort



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Receiver.Movement.ImportPy.ImportPyCls
	:members:
	:undoc-members:
	:noindex: