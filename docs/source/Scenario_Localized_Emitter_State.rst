State
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:EMITter:STATe:CLEar
	single: SCENario:LOCalized:EMITter:STATe:COUNt
	single: SCENario:LOCalized:EMITter:STATe:DELete
	single: SCENario:LOCalized:EMITter:STATe:DURation
	single: SCENario:LOCalized:EMITter:STATe:ENABle
	single: SCENario:LOCalized:EMITter:STATe:INSert
	single: SCENario:LOCalized:EMITter:STATe:SELect
	single: SCENario:LOCalized:EMITter:STATe:VALue

.. code-block:: python

	SCENario:LOCalized:EMITter:STATe:CLEar
	SCENario:LOCalized:EMITter:STATe:COUNt
	SCENario:LOCalized:EMITter:STATe:DELete
	SCENario:LOCalized:EMITter:STATe:DURation
	SCENario:LOCalized:EMITter:STATe:ENABle
	SCENario:LOCalized:EMITter:STATe:INSert
	SCENario:LOCalized:EMITter:STATe:SELect
	SCENario:LOCalized:EMITter:STATe:VALue



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Emitter.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.emitter.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Emitter_State_Add.rst