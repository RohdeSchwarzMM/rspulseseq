Mchg
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:MCHG:CLEar
	single: SCENario:LOCalized:MCHG:COUNt
	single: SCENario:LOCalized:MCHG:DELete
	single: SCENario:LOCalized:MCHG:SELect
	single: SCENario:LOCalized:MCHG:STARt
	single: SCENario:LOCalized:MCHG:STATe
	single: SCENario:LOCalized:MCHG:STOP

.. code-block:: python

	SCENario:LOCalized:MCHG:CLEar
	SCENario:LOCalized:MCHG:COUNt
	SCENario:LOCalized:MCHG:DELete
	SCENario:LOCalized:MCHG:SELect
	SCENario:LOCalized:MCHG:STARt
	SCENario:LOCalized:MCHG:STATe
	SCENario:LOCalized:MCHG:STOP



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Mchg.MchgCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.mchg.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Mchg_Add.rst