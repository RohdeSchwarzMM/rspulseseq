ClassPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:CLASs:ENABle

.. code-block:: python

	PROGram:CLASs:ENABle



.. autoclass:: RsPulseSeq.Implementations.Program.ClassPy.ClassPyCls
	:members:
	:undoc-members:
	:noindex: