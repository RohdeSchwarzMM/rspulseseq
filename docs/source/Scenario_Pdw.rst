Pdw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:PDW:ENABle
	single: SCENario:PDW:HOST
	single: SCENario:PDW:PATH
	single: SCENario:PDW:TEMPlate
	single: SCENario:PDW:TYPE

.. code-block:: python

	SCENario:PDW:ENABle
	SCENario:PDW:HOST
	SCENario:PDW:PATH
	SCENario:PDW:TEMPlate
	SCENario:PDW:TYPE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Pdw.PdwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.pdw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Pdw_AmMos.rst
	Scenario_Pdw_Plugin.rst