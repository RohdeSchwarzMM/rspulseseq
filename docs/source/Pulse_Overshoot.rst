Overshoot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:OVERshoot:DECay
	single: PULSe:OVERshoot

.. code-block:: python

	PULSe:OVERshoot:DECay
	PULSe:OVERshoot



.. autoclass:: RsPulseSeq.Implementations.Pulse.Overshoot.OvershootCls
	:members:
	:undoc-members:
	:noindex: