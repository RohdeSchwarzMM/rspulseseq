Circular
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:CIRCular:MODE
	single: SCAN:CIRCular:NELevation
	single: SCAN:CIRCular:NODDing
	single: SCAN:CIRCular:NRATe
	single: SCAN:CIRCular:PALMer
	single: SCAN:CIRCular:PERiod
	single: SCAN:CIRCular:PRATe
	single: SCAN:CIRCular:PSQuint
	single: SCAN:CIRCular:ROTation
	single: SCAN:CIRCular:RPM

.. code-block:: python

	SCAN:CIRCular:MODE
	SCAN:CIRCular:NELevation
	SCAN:CIRCular:NODDing
	SCAN:CIRCular:NRATe
	SCAN:CIRCular:PALMer
	SCAN:CIRCular:PERiod
	SCAN:CIRCular:PRATe
	SCAN:CIRCular:PSQuint
	SCAN:CIRCular:ROTation
	SCAN:CIRCular:RPM



.. autoclass:: RsPulseSeq.Implementations.Scan.Circular.CircularCls
	:members:
	:undoc-members:
	:noindex: