ImportPy
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.ImportPy.ImportPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.importPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	ImportPy_Pdw.rst
	ImportPy_View.rst