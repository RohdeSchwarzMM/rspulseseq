Count
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:REP:COUNt:DURation
	single: SEQuence:ITEM:REP:COUNt:FIXed
	single: SEQuence:ITEM:REP:COUNt:MAXimum
	single: SEQuence:ITEM:REP:COUNt:MINimum
	single: SEQuence:ITEM:REP:COUNt:STEP

.. code-block:: python

	SEQuence:ITEM:REP:COUNt:DURation
	SEQuence:ITEM:REP:COUNt:FIXed
	SEQuence:ITEM:REP:COUNt:MAXimum
	SEQuence:ITEM:REP:COUNt:MINimum
	SEQuence:ITEM:REP:COUNt:STEP



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Rep.Count.CountCls
	:members:
	:undoc-members:
	:noindex: