Bemitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:BEMitter:BWIDth
	single: WAVeform:BEMitter:COUNt
	single: WAVeform:BEMitter:DURation

.. code-block:: python

	WAVeform:BEMitter:BWIDth
	WAVeform:BEMitter:COUNt
	WAVeform:BEMitter:DURation



.. autoclass:: RsPulseSeq.Implementations.Waveform.Bemitter.BemitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.waveform.bemitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Waveform_Bemitter_Level.rst
	Waveform_Bemitter_Pri.rst
	Waveform_Bemitter_Pw.rst