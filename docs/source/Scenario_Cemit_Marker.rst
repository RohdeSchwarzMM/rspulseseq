Marker
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:MARKer:AUTO
	single: SCENario:CEMit:MARKer:FALL
	single: SCENario:CEMit:MARKer:FORCe
	single: SCENario:CEMit:MARKer:GATE
	single: SCENario:CEMit:MARKer:POST
	single: SCENario:CEMit:MARKer:PRE
	single: SCENario:CEMit:MARKer:RISE
	single: SCENario:CEMit:MARKer:WIDTh

.. code-block:: python

	SCENario:CEMit:MARKer:AUTO
	SCENario:CEMit:MARKer:FALL
	SCENario:CEMit:MARKer:FORCe
	SCENario:CEMit:MARKer:GATE
	SCENario:CEMit:MARKer:POST
	SCENario:CEMit:MARKer:PRE
	SCENario:CEMit:MARKer:RISE
	SCENario:CEMit:MARKer:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cemit.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cemit_Marker_Time.rst