Pstep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:LOCation:PSTep:COUNt
	single: SCENario:DF:LOCation:PSTep:DELete
	single: SCENario:DF:LOCation:PSTep:SELect

.. code-block:: python

	SCENario:DF:LOCation:PSTep:COUNt
	SCENario:DF:LOCation:PSTep:DELete
	SCENario:DF:LOCation:PSTep:SELect



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Location.Pstep.PstepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.location.pstep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Location_Pstep_Add.rst