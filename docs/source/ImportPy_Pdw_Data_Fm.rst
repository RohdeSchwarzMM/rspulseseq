Fm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:FM:DEViation
	single: IMPort:PDW:DATA:FM:MODFreq

.. code-block:: python

	IMPort:PDW:DATA:FM:DEViation
	IMPort:PDW:DATA:FM:MODFreq



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.Fm.FmCls
	:members:
	:undoc-members:
	:noindex: