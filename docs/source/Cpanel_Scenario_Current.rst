Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CPANel:SCENario:CURRent:NAME
	single: CPANel:SCENario:CURRent:STATus

.. code-block:: python

	CPANel:SCENario:CURRent:NAME
	CPANel:SCENario:CURRent:STATus



.. autoclass:: RsPulseSeq.Implementations.Cpanel.Scenario.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: