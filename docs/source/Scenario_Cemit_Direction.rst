Direction
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:DIRection:PITCh
	single: SCENario:CEMit:DIRection:ROLL
	single: SCENario:CEMit:DIRection:YAW

.. code-block:: python

	SCENario:CEMit:DIRection:PITCh
	SCENario:CEMit:DIRection:ROLL
	SCENario:CEMit:DIRection:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: