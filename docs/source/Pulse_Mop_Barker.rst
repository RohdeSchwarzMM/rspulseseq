Barker
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:BARKer:BLANk
	single: PULSe:MOP:BARKer:CODE
	single: PULSe:MOP:BARKer:TTIMe

.. code-block:: python

	PULSe:MOP:BARKer:BLANk
	PULSe:MOP:BARKer:CODE
	PULSe:MOP:BARKer:TTIMe



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Barker.BarkerCls
	:members:
	:undoc-members:
	:noindex: