Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:EMITter:ENABle
	single: SCENario:CEMit:EMITter

.. code-block:: python

	SCENario:CEMit:EMITter:ENABle
	SCENario:CEMit:EMITter



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cemit.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cemit_Emitter_Mode.rst