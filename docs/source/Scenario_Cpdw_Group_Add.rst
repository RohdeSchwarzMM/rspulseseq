Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:CPDW:GROup:ADD

.. code-block:: python

	SCENario:CPDW:GROup:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cpdw.Group.Add.AddCls
	:members:
	:undoc-members:
	:noindex: