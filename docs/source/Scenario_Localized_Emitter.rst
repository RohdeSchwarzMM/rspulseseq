Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:EMITter:ENABle
	single: SCENario:LOCalized:EMITter

.. code-block:: python

	SCENario:LOCalized:EMITter:ENABle
	SCENario:LOCalized:EMITter



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Emitter_Mode.rst
	Scenario_Localized_Emitter_State.rst