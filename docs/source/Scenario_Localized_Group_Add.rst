Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:GROup:ADD

.. code-block:: python

	SCENario:LOCalized:GROup:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Group.Add.AddCls
	:members:
	:undoc-members:
	:noindex: