Rec
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:LOCation:REC:PMODe

.. code-block:: python

	SCENario:LOCalized:LOCation:REC:PMODe



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Location.Rec.RecCls
	:members:
	:undoc-members:
	:noindex: