Rotation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:ROTation:X
	single: ANTenna:MODel:ROTation:Z

.. code-block:: python

	ANTenna:MODel:ROTation:X
	ANTenna:MODel:ROTation:Z



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Rotation.RotationCls
	:members:
	:undoc-members:
	:noindex: