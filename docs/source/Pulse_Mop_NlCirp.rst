NlCirp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:NLCHirp:EQUation

.. code-block:: python

	PULSe:MOP:NLCHirp:EQUation



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.NlCirp.NlCirpCls
	:members:
	:undoc-members:
	:noindex: