Rstep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:RSTep:MAXimum
	single: IPM:RSTep:MINimum
	single: IPM:RSTep:PERiod

.. code-block:: python

	IPM:RSTep:MAXimum
	IPM:RSTep:MINimum
	IPM:RSTep:PERiod



.. autoclass:: RsPulseSeq.Implementations.Ipm.Rstep.RstepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ipm.rstep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ipm_Rstep_Step.rst