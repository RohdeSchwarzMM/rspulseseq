Level
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: WAVeform:LEVel:REFerence

.. code-block:: python

	WAVeform:LEVel:REFerence



.. autoclass:: RsPulseSeq.Implementations.Waveform.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: