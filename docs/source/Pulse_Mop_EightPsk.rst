EightPsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:8PSK:SRATe

.. code-block:: python

	PULSe:MOP:8PSK:SRATe



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.EightPsk.EightPskCls
	:members:
	:undoc-members:
	:noindex: