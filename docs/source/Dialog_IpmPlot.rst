IpmPlot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIALog:IPMPlot:SAMPles
	single: DIALog:IPMPlot:VIEW

.. code-block:: python

	DIALog:IPMPlot:SAMPles
	DIALog:IPMPlot:VIEW



.. autoclass:: RsPulseSeq.Implementations.Dialog.IpmPlot.IpmPlotCls
	:members:
	:undoc-members:
	:noindex: