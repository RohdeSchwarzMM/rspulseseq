Level
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:EXCLude:LEVel:STARt
	single: PULSe:MOP:EXCLude:LEVel:STOP

.. code-block:: python

	PULSe:MOP:EXCLude:LEVel:STARt
	PULSe:MOP:EXCLude:LEVel:STOP



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Exclude.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: