Antenna
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:DESTination:PATH:ANTenna:CLEar
	single: ASSignment:DESTination:PATH:ANTenna:DELete
	single: ASSignment:DESTination:PATH:ANTenna:LIST
	single: ASSignment:DESTination:PATH:ANTenna:SELect

.. code-block:: python

	ASSignment:DESTination:PATH:ANTenna:CLEar
	ASSignment:DESTination:PATH:ANTenna:DELete
	ASSignment:DESTination:PATH:ANTenna:LIST
	ASSignment:DESTination:PATH:ANTenna:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Destination.Path.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.destination.path.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Destination_Path_Antenna_Add.rst