Mute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CPANel:MUTE

.. code-block:: python

	CPANel:MUTE



.. autoclass:: RsPulseSeq.Implementations.Cpanel.Mute.MuteCls
	:members:
	:undoc-members:
	:noindex: