Xpol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: REPository:XPOL:ATTenuation

.. code-block:: python

	REPository:XPOL:ATTenuation



.. autoclass:: RsPulseSeq.Implementations.Repository.Xpol.XpolCls
	:members:
	:undoc-members:
	:noindex: