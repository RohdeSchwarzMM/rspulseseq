Distribution
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:ARRay:DISTribution:TYPE
	single: ANTenna:MODel:ARRay:DISTribution:X
	single: ANTenna:MODel:ARRay:DISTribution:Z
	single: ANTenna:MODel:ARRay:DISTribution

.. code-block:: python

	ANTenna:MODel:ARRay:DISTribution:TYPE
	ANTenna:MODel:ARRay:DISTribution:X
	ANTenna:MODel:ARRay:DISTribution:Z
	ANTenna:MODel:ARRay:DISTribution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Array.Distribution.DistributionCls
	:members:
	:undoc-members:
	:noindex: