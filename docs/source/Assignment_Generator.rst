Generator
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:GENerator:CAPabilities
	single: ASSignment:GENerator:LIST
	single: ASSignment:GENerator:SELect

.. code-block:: python

	ASSignment:GENerator:CAPabilities
	ASSignment:GENerator:LIST
	ASSignment:GENerator:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Generator_Path.rst