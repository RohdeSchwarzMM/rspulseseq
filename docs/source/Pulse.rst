Pulse
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:CATalog
	single: PULSe:COMMent
	single: PULSe:CREate
	single: PULSe:CUSTom
	single: PULSe:NAME
	single: PULSe:REMove
	single: PULSe:SELect
	single: PULSe:SETTings

.. code-block:: python

	PULSe:CATalog
	PULSe:COMMent
	PULSe:CREate
	PULSe:CUSTom
	PULSe:NAME
	PULSe:REMove
	PULSe:SELect
	PULSe:SETTings



.. autoclass:: RsPulseSeq.Implementations.Pulse.PulseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Envelope.rst
	Pulse_Level.rst
	Pulse_Marker.rst
	Pulse_Mop.rst
	Pulse_Overshoot.rst
	Pulse_Preview.rst
	Pulse_Ripple.rst
	Pulse_Time.rst
	Pulse_TypePy.rst