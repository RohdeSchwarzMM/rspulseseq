Window
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: WAVeform:VIEW:OPEN:WINDow

.. code-block:: python

	WAVeform:VIEW:OPEN:WINDow



.. autoclass:: RsPulseSeq.Implementations.Waveform.View.Open.Window.WindowCls
	:members:
	:undoc-members:
	:noindex: