U
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:RANDom:U:CENTer
	single: IPM:RANDom:U:RANGe

.. code-block:: python

	IPM:RANDom:U:CENTer
	IPM:RANDom:U:RANGe



.. autoclass:: RsPulseSeq.Implementations.Ipm.Random.U.UCls
	:members:
	:undoc-members:
	:noindex: