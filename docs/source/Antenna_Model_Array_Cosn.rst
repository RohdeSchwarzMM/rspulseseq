Cosn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:ARRay:COSN:X
	single: ANTenna:MODel:ARRay:COSN:Z
	single: ANTenna:MODel:ARRay:COSN

.. code-block:: python

	ANTenna:MODel:ARRay:COSN:X
	ANTenna:MODel:ARRay:COSN:Z
	ANTenna:MODel:ARRay:COSN



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Array.Cosn.CosnCls
	:members:
	:undoc-members:
	:noindex: