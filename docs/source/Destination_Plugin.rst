Plugin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DESTination:PLUGin:NAME

.. code-block:: python

	DESTination:PLUGin:NAME



.. autoclass:: RsPulseSeq.Implementations.Destination.Plugin.PluginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.destination.plugin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Destination_Plugin_Variable.rst