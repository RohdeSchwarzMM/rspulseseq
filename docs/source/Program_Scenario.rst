Scenario
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Program.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.program.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Program_Scenario_Xtrg.rst