Apply
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: LSERver:APPLy

.. code-block:: python

	LSERver:APPLy



.. autoclass:: RsPulseSeq.Implementations.Lserver.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: