Ask
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:ASK:INVert
	single: PULSe:MOP:ASK:MDEPth
	single: PULSe:MOP:ASK:SRATe

.. code-block:: python

	PULSe:MOP:ASK:INVert
	PULSe:MOP:ASK:MDEPth
	PULSe:MOP:ASK:SRATe



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Ask.AskCls
	:members:
	:undoc-members:
	:noindex: