Model
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:BANDwidth
	single: ANTenna:MODel:FREQuency
	single: ANTenna:MODel:POLarization
	single: ANTenna:MODel:TYPE

.. code-block:: python

	ANTenna:MODel:BANDwidth
	ANTenna:MODel:FREQuency
	ANTenna:MODel:POLarization
	ANTenna:MODel:TYPE



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.ModelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_Array.rst
	Antenna_Model_Backlobe.rst
	Antenna_Model_Cardoid.rst
	Antenna_Model_Carray.rst
	Antenna_Model_Cosecant.rst
	Antenna_Model_Custom.rst
	Antenna_Model_Dipole.rst
	Antenna_Model_Gaussian.rst
	Antenna_Model_Horn.rst
	Antenna_Model_Parabolic.rst
	Antenna_Model_Plugin.rst
	Antenna_Model_Rotation.rst
	Antenna_Model_Sinc.rst
	Antenna_Model_User.rst