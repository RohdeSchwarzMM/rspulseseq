Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:PCHirp:ADD

.. code-block:: python

	PULSe:MOP:PCHirp:ADD



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Pchirp.Add.AddCls
	:members:
	:undoc-members:
	:noindex: