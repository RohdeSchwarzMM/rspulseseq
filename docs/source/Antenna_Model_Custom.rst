Custom
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CUSTom:RESolution
	single: ANTenna:MODel:CUSTom:SLRolloff
	single: ANTenna:MODel:CUSTom:SLSCale
	single: ANTenna:MODel:CUSTom:SLSTart

.. code-block:: python

	ANTenna:MODel:CUSTom:RESolution
	ANTenna:MODel:CUSTom:SLRolloff
	ANTenna:MODel:CUSTom:SLSCale
	ANTenna:MODel:CUSTom:SLSTart



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Custom.CustomCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.custom.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_Custom_HpBw.rst