Waypoint
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:RECeiver:MOVement:WAYPoint:CLEar
	single: SCENario:DF:RECeiver:MOVement:WAYPoint

.. code-block:: python

	SCENario:DF:RECeiver:MOVement:WAYPoint:CLEar
	SCENario:DF:RECeiver:MOVement:WAYPoint



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Receiver.Movement.Waypoint.WaypointCls
	:members:
	:undoc-members:
	:noindex: