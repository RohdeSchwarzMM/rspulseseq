Ask
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:ASK:CHIPcount
	single: IMPort:PDW:DATA:ASK:PATTern
	single: IMPort:PDW:DATA:ASK:RATE
	single: IMPort:PDW:DATA:ASK:STATes
	single: IMPort:PDW:DATA:ASK:STEP

.. code-block:: python

	IMPort:PDW:DATA:ASK:CHIPcount
	IMPort:PDW:DATA:ASK:PATTern
	IMPort:PDW:DATA:ASK:RATE
	IMPort:PDW:DATA:ASK:STATes
	IMPort:PDW:DATA:ASK:STEP



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.Ask.AskCls
	:members:
	:undoc-members:
	:noindex: