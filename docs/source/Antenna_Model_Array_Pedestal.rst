Pedestal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:ARRay:PEDestal:X
	single: ANTenna:MODel:ARRay:PEDestal:Z
	single: ANTenna:MODel:ARRay:PEDestal

.. code-block:: python

	ANTenna:MODel:ARRay:PEDestal:X
	ANTenna:MODel:ARRay:PEDestal:Z
	ANTenna:MODel:ARRay:PEDestal



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Array.Pedestal.PedestalCls
	:members:
	:undoc-members:
	:noindex: