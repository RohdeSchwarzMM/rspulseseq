Utime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:PDW:AMMos:UTIMe:ENABle
	single: SCENario:PDW:AMMos:UTIMe:ISO

.. code-block:: python

	SCENario:PDW:AMMos:UTIMe:ENABle
	SCENario:PDW:AMMos:UTIMe:ISO



.. autoclass:: RsPulseSeq.Implementations.Scenario.Pdw.AmMos.Utime.UtimeCls
	:members:
	:undoc-members:
	:noindex: