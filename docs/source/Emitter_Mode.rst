Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: EMITter:MODE:ID
	single: EMITter:MODE:CLEar
	single: EMITter:MODE:COUNt
	single: EMITter:MODE:DELete
	single: EMITter:MODE:NAME
	single: EMITter:MODE:SELect

.. code-block:: python

	EMITter:MODE:ID
	EMITter:MODE:CLEar
	EMITter:MODE:COUNt
	EMITter:MODE:DELete
	EMITter:MODE:NAME
	EMITter:MODE:SELect



.. autoclass:: RsPulseSeq.Implementations.Emitter.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.emitter.mode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Emitter_Mode_Add.rst
	Emitter_Mode_Antenna.rst
	Emitter_Mode_Beam.rst
	Emitter_Mode_Scan.rst