Repository
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: REPository:COMPlexity
	single: REPository:ACCess
	single: REPository:AUTHor
	single: REPository:CATalog
	single: REPository:COMMent
	single: REPository:CREate
	single: REPository:DATE
	single: REPository:FILename
	single: REPository:PATH
	single: REPository:REMove
	single: REPository:SAVE
	single: REPository:SECurity
	single: REPository:SELect
	single: REPository:UUID
	single: REPository:VERSion

.. code-block:: python

	REPository:COMPlexity
	REPository:ACCess
	REPository:AUTHor
	REPository:CATalog
	REPository:COMMent
	REPository:CREate
	REPository:DATE
	REPository:FILename
	REPository:PATH
	REPository:REMove
	REPository:SAVE
	REPository:SECurity
	REPository:SELect
	REPository:UUID
	REPository:VERSion



.. autoclass:: RsPulseSeq.Implementations.Repository.RepositoryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.repository.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Repository_Volatile.rst
	Repository_Xpol.rst