Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SEQuence:TIME:MODE

.. code-block:: python

	SEQuence:TIME:MODE



.. autoclass:: RsPulseSeq.Implementations.Sequence.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: