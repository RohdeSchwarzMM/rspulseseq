Plugin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IPM:PLUGin:NAME

.. code-block:: python

	IPM:PLUGin:NAME



.. autoclass:: RsPulseSeq.Implementations.Ipm.Plugin.PluginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ipm.plugin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ipm_Plugin_Variable.rst