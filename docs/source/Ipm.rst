Ipm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:CATalog
	single: IPM:COMMent
	single: IPM:CREate
	single: IPM:EQUation
	single: IPM:NAME
	single: IPM:REMove
	single: IPM:SELect
	single: IPM:TYPE
	single: IPM:UNIT

.. code-block:: python

	IPM:CATalog
	IPM:COMMent
	IPM:CREate
	IPM:EQUation
	IPM:NAME
	IPM:REMove
	IPM:SELect
	IPM:TYPE
	IPM:UNIT



.. autoclass:: RsPulseSeq.Implementations.Ipm.IpmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ipm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ipm_Binomial.rst
	Ipm_ListPy.rst
	Ipm_Plugin.rst
	Ipm_Random.rst
	Ipm_Rlist.rst
	Ipm_Rstep.rst
	Ipm_Shape.rst
	Ipm_Step.rst
	Ipm_Waveform.rst