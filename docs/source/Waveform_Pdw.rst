Pdw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: WAVeform:PDW:CENTer

.. code-block:: python

	WAVeform:PDW:CENTer



.. autoclass:: RsPulseSeq.Implementations.Waveform.Pdw.PdwCls
	:members:
	:undoc-members:
	:noindex: