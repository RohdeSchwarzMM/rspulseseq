Direction
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PLATform:EMITter:DIRection:AWAY

.. code-block:: python

	PLATform:EMITter:DIRection:AWAY



.. autoclass:: RsPulseSeq.Implementations.Platform.Emitter.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: