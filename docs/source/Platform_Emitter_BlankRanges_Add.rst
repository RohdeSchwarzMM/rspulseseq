Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PLATform:EMITter:BLANkranges:ADD

.. code-block:: python

	PLATform:EMITter:BLANkranges:ADD



.. autoclass:: RsPulseSeq.Implementations.Platform.Emitter.BlankRanges.Add.AddCls
	:members:
	:undoc-members:
	:noindex: