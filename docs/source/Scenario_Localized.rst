Localized
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:ALIas
	single: SCENario:LOCalized:CLEar
	single: SCENario:LOCalized:CURRent
	single: SCENario:LOCalized:DELete
	single: SCENario:LOCalized:DISTance
	single: SCENario:LOCalized:ENABle
	single: SCENario:LOCalized:FREQuency
	single: SCENario:LOCalized:LDELay
	single: SCENario:LOCalized:LEVel
	single: SCENario:LOCalized:PRIority
	single: SCENario:LOCalized:SELect
	single: SCENario:LOCalized:SEQuence
	single: SCENario:LOCalized:THReshold
	single: SCENario:LOCalized:TYPE

.. code-block:: python

	SCENario:LOCalized:ALIas
	SCENario:LOCalized:CLEar
	SCENario:LOCalized:CURRent
	SCENario:LOCalized:DELete
	SCENario:LOCalized:DISTance
	SCENario:LOCalized:ENABle
	SCENario:LOCalized:FREQuency
	SCENario:LOCalized:LDELay
	SCENario:LOCalized:LEVel
	SCENario:LOCalized:PRIority
	SCENario:LOCalized:SELect
	SCENario:LOCalized:SEQuence
	SCENario:LOCalized:THReshold
	SCENario:LOCalized:TYPE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.LocalizedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Add.rst
	Scenario_Localized_Direction.rst
	Scenario_Localized_Emitter.rst
	Scenario_Localized_Group.rst
	Scenario_Localized_Interleaving.rst
	Scenario_Localized_Location.rst
	Scenario_Localized_Maps.rst
	Scenario_Localized_Marker.rst
	Scenario_Localized_Mchg.rst
	Scenario_Localized_Movement.rst
	Scenario_Localized_Receiver.rst
	Scenario_Localized_Subitem.rst
	Scenario_Localized_Waveform.rst