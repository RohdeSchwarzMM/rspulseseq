Dsrc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DSRC:CATalog
	single: DSRC:CLEar
	single: DSRC:COMMent
	single: DSRC:CREate
	single: DSRC:NAME
	single: DSRC:REMove
	single: DSRC:SELect

.. code-block:: python

	DSRC:CATalog
	DSRC:CLEar
	DSRC:COMMent
	DSRC:CREate
	DSRC:NAME
	DSRC:REMove
	DSRC:SELect



.. autoclass:: RsPulseSeq.Implementations.Dsrc.DsrcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.dsrc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Dsrc_Item.rst