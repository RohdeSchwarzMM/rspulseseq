Pdw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:FILE:PDW:LOAD
	single: IMPort:PDW:FILE:PDW:SAVE
	single: IMPort:PDW:FILE:PDW

.. code-block:: python

	IMPort:PDW:FILE:PDW:LOAD
	IMPort:PDW:FILE:PDW:SAVE
	IMPort:PDW:FILE:PDW



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.File.Pdw.PdwCls
	:members:
	:undoc-members:
	:noindex: