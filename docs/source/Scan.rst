Scan
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:CATalog
	single: SCAN:COMMent
	single: SCAN:CREate
	single: SCAN:NAME
	single: SCAN:REMove
	single: SCAN:SELect
	single: SCAN:STEering
	single: SCAN:TYPE

.. code-block:: python

	SCAN:CATalog
	SCAN:COMMent
	SCAN:CREate
	SCAN:NAME
	SCAN:REMove
	SCAN:SELect
	SCAN:STEering
	SCAN:TYPE



.. autoclass:: RsPulseSeq.Implementations.Scan.ScanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scan_Circular.rst
	Scan_Conical.rst
	Scan_Custom.rst
	Scan_Helical.rst
	Scan_Lissajous.rst
	Scan_Lsw.rst
	Scan_Raster.rst
	Scan_Sector.rst
	Scan_Sin.rst
	Scan_Spiral.rst