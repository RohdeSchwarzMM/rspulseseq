ImportPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:MOVement:IMPort

.. code-block:: python

	SCENario:DF:MOVement:IMPort



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Movement.ImportPy.ImportPyCls
	:members:
	:undoc-members:
	:noindex: