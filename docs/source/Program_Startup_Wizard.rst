Wizard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:STARtup:WIZard:ENABle

.. code-block:: python

	PROGram:STARtup:WIZard:ENABle



.. autoclass:: RsPulseSeq.Implementations.Program.Startup.Wizard.WizardCls
	:members:
	:undoc-members:
	:noindex: