PlFm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IMPort:PDW:DATA:PLFM:VALues

.. code-block:: python

	IMPort:PDW:DATA:PLFM:VALues



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.PlFm.PlFmCls
	:members:
	:undoc-members:
	:noindex: