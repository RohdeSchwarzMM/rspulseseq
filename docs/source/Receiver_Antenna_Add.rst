Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: RECeiver:ANTenna:ADD

.. code-block:: python

	RECeiver:ANTenna:ADD



.. autoclass:: RsPulseSeq.Implementations.Receiver.Antenna.Add.AddCls
	:members:
	:undoc-members:
	:noindex: