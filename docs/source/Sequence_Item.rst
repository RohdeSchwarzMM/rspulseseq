Item
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:CLEar
	single: SEQuence:ITEM:COUNt
	single: SEQuence:ITEM:DELete
	single: SEQuence:ITEM:INDent
	single: SEQuence:ITEM:PDELay
	single: SEQuence:ITEM:PRF
	single: SEQuence:ITEM:PRI
	single: SEQuence:ITEM:PULSe
	single: SEQuence:ITEM:SELect
	single: SEQuence:ITEM:TYPE
	single: SEQuence:ITEM:WAVeform

.. code-block:: python

	SEQuence:ITEM:CLEar
	SEQuence:ITEM:COUNt
	SEQuence:ITEM:DELete
	SEQuence:ITEM:INDent
	SEQuence:ITEM:PDELay
	SEQuence:ITEM:PRF
	SEQuence:ITEM:PRI
	SEQuence:ITEM:PULSe
	SEQuence:ITEM:SELect
	SEQuence:ITEM:TYPE
	SEQuence:ITEM:WAVeform



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.ItemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sequence.item.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sequence_Item_Add.rst
	Sequence_Item_Filler.rst
	Sequence_Item_Frequency.rst
	Sequence_Item_Ipm.rst
	Sequence_Item_Level.rst
	Sequence_Item_Loop.rst
	Sequence_Item_Marker.rst
	Sequence_Item_Ovl.rst
	Sequence_Item_Phase.rst
	Sequence_Item_Rep.rst