Waveform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:WAVeform:BASE
	single: IPM:WAVeform:COUNt
	single: IPM:WAVeform:OFFSet
	single: IPM:WAVeform:PERiod
	single: IPM:WAVeform:PHASe
	single: IPM:WAVeform:PKPK
	single: IPM:WAVeform:TYPE

.. code-block:: python

	IPM:WAVeform:BASE
	IPM:WAVeform:COUNt
	IPM:WAVeform:OFFSet
	IPM:WAVeform:PERiod
	IPM:WAVeform:PHASe
	IPM:WAVeform:PKPK
	IPM:WAVeform:TYPE



.. autoclass:: RsPulseSeq.Implementations.Ipm.Waveform.WaveformCls
	:members:
	:undoc-members:
	:noindex: