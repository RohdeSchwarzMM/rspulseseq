Msk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:MSK:INVert
	single: PULSe:MOP:MSK:SRATe

.. code-block:: python

	PULSe:MOP:MSK:INVert
	PULSe:MOP:MSK:SRATe



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Msk.MskCls
	:members:
	:undoc-members:
	:noindex: