FilterPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:FILTer:BT
	single: PULSe:MOP:FILTer:BWIDth
	single: PULSe:MOP:FILTer:LENGth
	single: PULSe:MOP:FILTer:ROLLoff
	single: PULSe:MOP:FILTer:TYPE

.. code-block:: python

	PULSe:MOP:FILTer:BT
	PULSe:MOP:FILTer:BWIDth
	PULSe:MOP:FILTer:LENGth
	PULSe:MOP:FILTer:ROLLoff
	PULSe:MOP:FILTer:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: