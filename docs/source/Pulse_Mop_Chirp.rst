Chirp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:CHIRp:DEViation
	single: PULSe:MOP:CHIRp:TYPE

.. code-block:: python

	PULSe:MOP:CHIRp:DEViation
	PULSe:MOP:CHIRp:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Chirp.ChirpCls
	:members:
	:undoc-members:
	:noindex: