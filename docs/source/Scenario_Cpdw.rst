Cpdw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CPDW:ALIas
	single: SCENario:CPDW:CLEar
	single: SCENario:CPDW:DELete
	single: SCENario:CPDW:ENABle
	single: SCENario:CPDW:FREQ
	single: SCENario:CPDW:INTerleaving
	single: SCENario:CPDW:LDELay
	single: SCENario:CPDW:LEVel
	single: SCENario:CPDW:LVABs
	single: SCENario:CPDW:NAME
	single: SCENario:CPDW:PRIority
	single: SCENario:CPDW:SELect
	single: SCENario:CPDW:THReshold

.. code-block:: python

	SCENario:CPDW:ALIas
	SCENario:CPDW:CLEar
	SCENario:CPDW:DELete
	SCENario:CPDW:ENABle
	SCENario:CPDW:FREQ
	SCENario:CPDW:INTerleaving
	SCENario:CPDW:LDELay
	SCENario:CPDW:LEVel
	SCENario:CPDW:LVABs
	SCENario:CPDW:NAME
	SCENario:CPDW:PRIority
	SCENario:CPDW:SELect
	SCENario:CPDW:THReshold



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cpdw.CpdwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.cpdw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Cpdw_Add.rst
	Scenario_Cpdw_Group.rst