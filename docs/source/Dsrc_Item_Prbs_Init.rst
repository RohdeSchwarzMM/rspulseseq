Init
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DSRC:ITEM:PRBS:INIT:VALue
	single: DSRC:ITEM:PRBS:INIT

.. code-block:: python

	DSRC:ITEM:PRBS:INIT:VALue
	DSRC:ITEM:PRBS:INIT



.. autoclass:: RsPulseSeq.Implementations.Dsrc.Item.Prbs.Init.InitCls
	:members:
	:undoc-members:
	:noindex: