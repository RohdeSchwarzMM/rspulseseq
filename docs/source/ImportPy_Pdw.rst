Pdw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:NORM
	single: IMPort:PDW:STATus

.. code-block:: python

	IMPort:PDW:NORM
	IMPort:PDW:STATus



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.PdwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.importPy.pdw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	ImportPy_Pdw_Data.rst
	ImportPy_Pdw_Execute.rst
	ImportPy_Pdw_File.rst
	ImportPy_Pdw_Store.rst