Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:PIECewise:ADD

.. code-block:: python

	PULSe:MOP:PIECewise:ADD



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Piecewise.Add.AddCls
	:members:
	:undoc-members:
	:noindex: