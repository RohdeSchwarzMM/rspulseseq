Antenna
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: EMITter:MODE:ANTenna:CLEar
	single: EMITter:MODE:ANTenna

.. code-block:: python

	EMITter:MODE:ANTenna:CLEar
	EMITter:MODE:ANTenna



.. autoclass:: RsPulseSeq.Implementations.Emitter.Mode.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex: