Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DSRC:ITEM:PRBS:TYPE

.. code-block:: python

	DSRC:ITEM:PRBS:TYPE



.. autoclass:: RsPulseSeq.Implementations.Dsrc.Item.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.dsrc.item.prbs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Dsrc_Item_Prbs_Init.rst