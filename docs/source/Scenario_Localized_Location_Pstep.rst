Pstep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:LOCation:PSTep:COUNt
	single: SCENario:LOCalized:LOCation:PSTep:DELete
	single: SCENario:LOCalized:LOCation:PSTep:SELect

.. code-block:: python

	SCENario:LOCalized:LOCation:PSTep:COUNt
	SCENario:LOCalized:LOCation:PSTep:DELete
	SCENario:LOCalized:LOCation:PSTep:SELect



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Location.Pstep.PstepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.location.pstep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Location_Pstep_Add.rst