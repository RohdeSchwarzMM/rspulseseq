Psk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:PSK:CHIPcount
	single: IMPort:PDW:DATA:PSK:PATTern
	single: IMPort:PDW:DATA:PSK:RATE
	single: IMPort:PDW:DATA:PSK:STATes
	single: IMPort:PDW:DATA:PSK:STEP

.. code-block:: python

	IMPort:PDW:DATA:PSK:CHIPcount
	IMPort:PDW:DATA:PSK:PATTern
	IMPort:PDW:DATA:PSK:RATE
	IMPort:PDW:DATA:PSK:STATes
	IMPort:PDW:DATA:PSK:STEP



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.Psk.PskCls
	:members:
	:undoc-members:
	:noindex: