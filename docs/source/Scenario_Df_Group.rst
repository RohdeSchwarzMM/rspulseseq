Group
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:GROup:ALIas
	single: SCENario:DF:GROup:CATalog
	single: SCENario:DF:GROup:CLEar
	single: SCENario:DF:GROup:COUNt
	single: SCENario:DF:GROup:DELete
	single: SCENario:DF:GROup:SELect
	single: SCENario:DF:GROup

.. code-block:: python

	SCENario:DF:GROup:ALIas
	SCENario:DF:GROup:CATalog
	SCENario:DF:GROup:CLEar
	SCENario:DF:GROup:COUNt
	SCENario:DF:GROup:DELete
	SCENario:DF:GROup:SELect
	SCENario:DF:GROup



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Group.GroupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.group.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Group_Add.rst