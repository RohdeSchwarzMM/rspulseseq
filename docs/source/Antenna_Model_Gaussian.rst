Gaussian
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ANTenna:MODel:GAUSsian:RESolution

.. code-block:: python

	ANTenna:MODel:GAUSsian:RESolution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Gaussian.GaussianCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.gaussian.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_Gaussian_HpBw.rst