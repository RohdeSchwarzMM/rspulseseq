Direction
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:EMITter:DIRection:PITCh
	single: SCENario:EMITter:DIRection:ROLL
	single: SCENario:EMITter:DIRection:YAW

.. code-block:: python

	SCENario:EMITter:DIRection:PITCh
	SCENario:EMITter:DIRection:ROLL
	SCENario:EMITter:DIRection:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Emitter.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: