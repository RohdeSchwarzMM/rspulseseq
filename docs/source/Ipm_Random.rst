Random
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IPM:RANDom:DISTribution

.. code-block:: python

	IPM:RANDom:DISTribution



.. autoclass:: RsPulseSeq.Implementations.Ipm.Random.RandomCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ipm.random.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ipm_Random_Normal.rst
	Ipm_Random_U.rst
	Ipm_Random_Uniform.rst