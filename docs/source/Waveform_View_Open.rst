Open
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Waveform.View.Open.OpenCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.waveform.view.open.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Waveform_View_Open_Window.rst