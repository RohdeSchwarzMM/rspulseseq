Antenna
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:GENerator:PATH:ANTenna:CLEar
	single: ASSignment:GENerator:PATH:ANTenna:DELete
	single: ASSignment:GENerator:PATH:ANTenna:LIST
	single: ASSignment:GENerator:PATH:ANTenna:SELect

.. code-block:: python

	ASSignment:GENerator:PATH:ANTenna:CLEar
	ASSignment:GENerator:PATH:ANTenna:DELete
	ASSignment:GENerator:PATH:ANTenna:LIST
	ASSignment:GENerator:PATH:ANTenna:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Generator.Path.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.generator.path.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Generator_Path_Antenna_Add.rst