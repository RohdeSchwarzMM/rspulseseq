Step
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:RSTep:STEP:MAXimum
	single: IPM:RSTep:STEP:MINimum

.. code-block:: python

	IPM:RSTep:STEP:MAXimum
	IPM:RSTep:STEP:MINimum



.. autoclass:: RsPulseSeq.Implementations.Ipm.Rstep.Step.StepCls
	:members:
	:undoc-members:
	:noindex: