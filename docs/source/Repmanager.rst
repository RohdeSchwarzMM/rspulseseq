Repmanager
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: REPManager:CATalog
	single: REPManager:DELete
	single: REPManager:EXPort
	single: REPManager:LOAD

.. code-block:: python

	REPManager:CATalog
	REPManager:DELete
	REPManager:EXPort
	REPManager:LOAD



.. autoclass:: RsPulseSeq.Implementations.Repmanager.RepmanagerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.repmanager.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Repmanager_Path.rst