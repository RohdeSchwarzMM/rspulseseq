Location
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:LOCation:ALTitude
	single: SCENario:LOCalized:LOCation:AZIMuth
	single: SCENario:LOCalized:LOCation:EAST
	single: SCENario:LOCalized:LOCation:ELEVation
	single: SCENario:LOCalized:LOCation:HEIGht
	single: SCENario:LOCalized:LOCation:LATitude
	single: SCENario:LOCalized:LOCation:LONGitude
	single: SCENario:LOCalized:LOCation:NORTh
	single: SCENario:LOCalized:LOCation:PMODe

.. code-block:: python

	SCENario:LOCalized:LOCation:ALTitude
	SCENario:LOCalized:LOCation:AZIMuth
	SCENario:LOCalized:LOCation:EAST
	SCENario:LOCalized:LOCation:ELEVation
	SCENario:LOCalized:LOCation:HEIGht
	SCENario:LOCalized:LOCation:LATitude
	SCENario:LOCalized:LOCation:LONGitude
	SCENario:LOCalized:LOCation:NORTh
	SCENario:LOCalized:LOCation:PMODe



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Location.LocationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.location.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Location_Pstep.rst
	Scenario_Localized_Location_Rec.rst
	Scenario_Localized_Location_Waypoint.rst