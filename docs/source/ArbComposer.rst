ArbComposer
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.ArbComposer.ArbComposerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.arbComposer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	ArbComposer_Show.rst