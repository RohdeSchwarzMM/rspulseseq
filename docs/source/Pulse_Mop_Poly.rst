Poly
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:POLY:LENGth
	single: PULSe:MOP:POLY:TYPE

.. code-block:: python

	PULSe:MOP:POLY:LENGth
	PULSe:MOP:POLY:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Poly.PolyCls
	:members:
	:undoc-members:
	:noindex: