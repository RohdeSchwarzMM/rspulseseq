ImportPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:RECeiver:MOVement:IMPort

.. code-block:: python

	SCENario:DF:RECeiver:MOVement:IMPort



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Receiver.Movement.ImportPy.ImportPyCls
	:members:
	:undoc-members:
	:noindex: