Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:DESTination:PATH:EMITter:CLEar
	single: ASSignment:DESTination:PATH:EMITter:DELete
	single: ASSignment:DESTination:PATH:EMITter:LIST
	single: ASSignment:DESTination:PATH:EMITter:SELect

.. code-block:: python

	ASSignment:DESTination:PATH:EMITter:CLEar
	ASSignment:DESTination:PATH:EMITter:DELete
	ASSignment:DESTination:PATH:EMITter:LIST
	ASSignment:DESTination:PATH:EMITter:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Destination.Path.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.destination.path.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Destination_Path_Emitter_Add.rst