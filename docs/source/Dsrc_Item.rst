Item
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DSRC:ITEM:BITS
	single: DSRC:ITEM:DATA
	single: DSRC:ITEM:DELete
	single: DSRC:ITEM:PATTern
	single: DSRC:ITEM:SELect
	single: DSRC:ITEM:TYPE

.. code-block:: python

	DSRC:ITEM:BITS
	DSRC:ITEM:DATA
	DSRC:ITEM:DELete
	DSRC:ITEM:PATTern
	DSRC:ITEM:SELect
	DSRC:ITEM:TYPE



.. autoclass:: RsPulseSeq.Implementations.Dsrc.Item.ItemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.dsrc.item.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Dsrc_Item_Add.rst
	Dsrc_Item_Prbs.rst