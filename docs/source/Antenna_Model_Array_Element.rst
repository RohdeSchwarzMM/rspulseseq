Element
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ANTenna:MODel:ARRay:ELEMent:COSine

.. code-block:: python

	ANTenna:MODel:ARRay:ELEMent:COSine



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Array.Element.ElementCls
	:members:
	:undoc-members:
	:noindex: