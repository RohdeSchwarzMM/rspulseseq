Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SEQuence:ITEM:IPM:ADD

.. code-block:: python

	SEQuence:ITEM:IPM:ADD



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Ipm.Add.AddCls
	:members:
	:undoc-members:
	:noindex: