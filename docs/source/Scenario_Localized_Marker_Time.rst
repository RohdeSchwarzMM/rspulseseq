Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:MARKer:TIME:POST
	single: SCENario:LOCalized:MARKer:TIME:PRE

.. code-block:: python

	SCENario:LOCalized:MARKer:TIME:POST
	SCENario:LOCalized:MARKer:TIME:PRE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Marker.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: