Select
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:PLUGin:VARiable:SELect:ID
	single: IPM:PLUGin:VARiable:SELect

.. code-block:: python

	IPM:PLUGin:VARiable:SELect:ID
	IPM:PLUGin:VARiable:SELect



.. autoclass:: RsPulseSeq.Implementations.Ipm.Plugin.Variable.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: