Antennas
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:ANTennas:LIST
	single: ASSignment:ANTennas:SELect

.. code-block:: python

	ASSignment:ANTennas:LIST
	ASSignment:ANTennas:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Antennas.AntennasCls
	:members:
	:undoc-members:
	:noindex: