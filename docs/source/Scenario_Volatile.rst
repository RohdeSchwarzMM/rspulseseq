Volatile
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:VOLatile:SEL

.. code-block:: python

	SCENario:VOLatile:SEL



.. autoclass:: RsPulseSeq.Implementations.Scenario.Volatile.VolatileCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.volatile.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Volatile_View.rst