Interleave
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:INTerleave

.. code-block:: python

	SCENario:INTerleave



.. autoclass:: RsPulseSeq.Implementations.Scenario.Interleave.InterleaveCls
	:members:
	:undoc-members:
	:noindex: