Select
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:PDW:PLUGin:VARiable:SELect:ID
	single: SCENario:PDW:PLUGin:VARiable:SELect

.. code-block:: python

	SCENario:PDW:PLUGin:VARiable:SELect:ID
	SCENario:PDW:PLUGin:VARiable:SELect



.. autoclass:: RsPulseSeq.Implementations.Scenario.Pdw.Plugin.Variable.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: