Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:EMITter:STATe:ADD

.. code-block:: python

	SCENario:LOCalized:EMITter:STATe:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Emitter.State.Add.AddCls
	:members:
	:undoc-members:
	:noindex: