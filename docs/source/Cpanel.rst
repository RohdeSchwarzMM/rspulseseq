Cpanel
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CPANel:ACTivate
	single: CPANel:DEACtivate
	single: CPANel:SETup

.. code-block:: python

	CPANel:ACTivate
	CPANel:DEACtivate
	CPANel:SETup



.. autoclass:: RsPulseSeq.Implementations.Cpanel.CpanelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.cpanel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Cpanel_Mute.rst
	Cpanel_Refresh.rst
	Cpanel_Scenario.rst
	Cpanel_Unmute.rst
	Cpanel_Unused.rst
	Cpanel_Used.rst