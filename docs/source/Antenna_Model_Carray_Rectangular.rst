Rectangular
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:RECTangular:LATTice
	single: ANTenna:MODel:CARRay:RECTangular:NX
	single: ANTenna:MODel:CARRay:RECTangular:NZ
	single: ANTenna:MODel:CARRay:RECTangular:XDIStance
	single: ANTenna:MODel:CARRay:RECTangular:ZDIStance

.. code-block:: python

	ANTenna:MODel:CARRay:RECTangular:LATTice
	ANTenna:MODel:CARRay:RECTangular:NX
	ANTenna:MODel:CARRay:RECTangular:NZ
	ANTenna:MODel:CARRay:RECTangular:XDIStance
	ANTenna:MODel:CARRay:RECTangular:ZDIStance



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Rectangular.RectangularCls
	:members:
	:undoc-members:
	:noindex: