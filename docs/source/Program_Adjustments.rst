Adjustments
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:ADJustments:ENABle

.. code-block:: python

	PROGram:ADJustments:ENABle



.. autoclass:: RsPulseSeq.Implementations.Program.Adjustments.AdjustmentsCls
	:members:
	:undoc-members:
	:noindex: