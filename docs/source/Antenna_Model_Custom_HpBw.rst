HpBw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CUSTom:HPBW:XY
	single: ANTenna:MODel:CUSTom:HPBW:YZ

.. code-block:: python

	ANTenna:MODel:CUSTom:HPBW:XY
	ANTenna:MODel:CUSTom:HPBW:YZ



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Custom.HpBw.HpBwCls
	:members:
	:undoc-members:
	:noindex: