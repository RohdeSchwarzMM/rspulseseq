Select
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DESTination:PLUGin:VARiable:SELect:ID
	single: DESTination:PLUGin:VARiable:SELect

.. code-block:: python

	DESTination:PLUGin:VARiable:SELect:ID
	DESTination:PLUGin:VARiable:SELect



.. autoclass:: RsPulseSeq.Implementations.Destination.Plugin.Variable.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: