Movement
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:RECeiver:MOVement:ACCeleration
	single: SCENario:LOCalized:RECeiver:MOVement:ANGLe
	single: SCENario:LOCalized:RECeiver:MOVement:ATTitude
	single: SCENario:LOCalized:RECeiver:MOVement:CLEar
	single: SCENario:LOCalized:RECeiver:MOVement:EAST
	single: SCENario:LOCalized:RECeiver:MOVement:HEIGht
	single: SCENario:LOCalized:RECeiver:MOVement:NORTh
	single: SCENario:LOCalized:RECeiver:MOVement:PITCh
	single: SCENario:LOCalized:RECeiver:MOVement:RFRame
	single: SCENario:LOCalized:RECeiver:MOVement:RMODe
	single: SCENario:LOCalized:RECeiver:MOVement:ROLL
	single: SCENario:LOCalized:RECeiver:MOVement:SMOothening
	single: SCENario:LOCalized:RECeiver:MOVement:SPEed
	single: SCENario:LOCalized:RECeiver:MOVement:SPINning
	single: SCENario:LOCalized:RECeiver:MOVement:TYPE
	single: SCENario:LOCalized:RECeiver:MOVement:VEHicle
	single: SCENario:LOCalized:RECeiver:MOVement:YAW

.. code-block:: python

	SCENario:LOCalized:RECeiver:MOVement:ACCeleration
	SCENario:LOCalized:RECeiver:MOVement:ANGLe
	SCENario:LOCalized:RECeiver:MOVement:ATTitude
	SCENario:LOCalized:RECeiver:MOVement:CLEar
	SCENario:LOCalized:RECeiver:MOVement:EAST
	SCENario:LOCalized:RECeiver:MOVement:HEIGht
	SCENario:LOCalized:RECeiver:MOVement:NORTh
	SCENario:LOCalized:RECeiver:MOVement:PITCh
	SCENario:LOCalized:RECeiver:MOVement:RFRame
	SCENario:LOCalized:RECeiver:MOVement:RMODe
	SCENario:LOCalized:RECeiver:MOVement:ROLL
	SCENario:LOCalized:RECeiver:MOVement:SMOothening
	SCENario:LOCalized:RECeiver:MOVement:SPEed
	SCENario:LOCalized:RECeiver:MOVement:SPINning
	SCENario:LOCalized:RECeiver:MOVement:TYPE
	SCENario:LOCalized:RECeiver:MOVement:VEHicle
	SCENario:LOCalized:RECeiver:MOVement:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Receiver.Movement.MovementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.receiver.movement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Receiver_Movement_ImportPy.rst
	Scenario_Localized_Receiver_Movement_Pstep.rst
	Scenario_Localized_Receiver_Movement_Vfile.rst
	Scenario_Localized_Receiver_Movement_Waypoint.rst