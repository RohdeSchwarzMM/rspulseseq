Pstep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:DF:RECeiver:MOVement:PSTep:SELect

.. code-block:: python

	SCENario:DF:RECeiver:MOVement:PSTep:SELect



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Receiver.Movement.Pstep.PstepCls
	:members:
	:undoc-members:
	:noindex: