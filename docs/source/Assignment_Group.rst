Group
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:GROup:LIST
	single: ASSignment:GROup:SELect

.. code-block:: python

	ASSignment:GROup:LIST
	ASSignment:GROup:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Group.GroupCls
	:members:
	:undoc-members:
	:noindex: