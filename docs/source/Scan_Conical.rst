Conical
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:CONical:RATE
	single: SCAN:CONical:ROTation
	single: SCAN:CONical:SQUint

.. code-block:: python

	SCAN:CONical:RATE
	SCAN:CONical:ROTation
	SCAN:CONical:SQUint



.. autoclass:: RsPulseSeq.Implementations.Scan.Conical.ConicalCls
	:members:
	:undoc-members:
	:noindex: