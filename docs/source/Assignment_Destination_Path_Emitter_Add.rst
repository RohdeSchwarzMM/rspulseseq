Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ASSignment:DESTination:PATH:EMITter:ADD

.. code-block:: python

	ASSignment:DESTination:PATH:EMITter:ADD



.. autoclass:: RsPulseSeq.Implementations.Assignment.Destination.Path.Emitter.Add.AddCls
	:members:
	:undoc-members:
	:noindex: