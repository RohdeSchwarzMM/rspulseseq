Cosn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:COSN:X
	single: ANTenna:MODel:CARRay:COSN:Z
	single: ANTenna:MODel:CARRay:COSN

.. code-block:: python

	ANTenna:MODel:CARRay:COSN:X
	ANTenna:MODel:CARRay:COSN:Z
	ANTenna:MODel:CARRay:COSN



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.Cosn.CosnCls
	:members:
	:undoc-members:
	:noindex: