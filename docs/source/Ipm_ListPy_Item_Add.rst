Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IPM:LIST:ITEM:ADD

.. code-block:: python

	IPM:LIST:ITEM:ADD



.. autoclass:: RsPulseSeq.Implementations.Ipm.ListPy.Item.Add.AddCls
	:members:
	:undoc-members:
	:noindex: