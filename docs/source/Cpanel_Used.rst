Used
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CPANel:USED:FREQuency
	single: CPANel:USED:LEVel
	single: CPANel:USED:LIST
	single: CPANel:USED:SELect
	single: CPANel:USED:STATe

.. code-block:: python

	CPANel:USED:FREQuency
	CPANel:USED:LEVel
	CPANel:USED:LIST
	CPANel:USED:SELect
	CPANel:USED:STATe



.. autoclass:: RsPulseSeq.Implementations.Cpanel.Used.UsedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.cpanel.used.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Cpanel_Used_Path.rst