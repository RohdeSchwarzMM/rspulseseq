Plugin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:PLUGin:NAME

.. code-block:: python

	PULSe:MOP:PLUGin:NAME



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Plugin.PluginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.plugin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Plugin_Variable.rst