File
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.File.FileCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.importPy.pdw.file.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	ImportPy_Pdw_File_Pdw.rst
	ImportPy_Pdw_File_Template.rst