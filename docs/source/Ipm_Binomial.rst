Binomial
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:BINomial:PVAL1
	single: IPM:BINomial:VAL1
	single: IPM:BINomial:VAL2

.. code-block:: python

	IPM:BINomial:PVAL1
	IPM:BINomial:VAL1
	IPM:BINomial:VAL2



.. autoclass:: RsPulseSeq.Implementations.Ipm.Binomial.BinomialCls
	:members:
	:undoc-members:
	:noindex: