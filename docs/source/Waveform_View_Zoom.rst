Zoom
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:VIEW:ZOOM:POINt
	single: WAVeform:VIEW:ZOOM:RANGe

.. code-block:: python

	WAVeform:VIEW:ZOOM:POINt
	WAVeform:VIEW:ZOOM:RANGe



.. autoclass:: RsPulseSeq.Implementations.Waveform.View.Zoom.ZoomCls
	:members:
	:undoc-members:
	:noindex: