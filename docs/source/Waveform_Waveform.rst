Waveform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:WAVeform:CLEar
	single: WAVeform:WAVeform:LOAD

.. code-block:: python

	WAVeform:WAVeform:CLEar
	WAVeform:WAVeform:LOAD



.. autoclass:: RsPulseSeq.Implementations.Waveform.Waveform.WaveformCls
	:members:
	:undoc-members:
	:noindex: