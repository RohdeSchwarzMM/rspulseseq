Am
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:AM:FREQuency
	single: PULSe:MOP:AM:MDEPth
	single: PULSe:MOP:AM:TYPE

.. code-block:: python

	PULSe:MOP:AM:FREQuency
	PULSe:MOP:AM:MDEPth
	PULSe:MOP:AM:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Am.AmCls
	:members:
	:undoc-members:
	:noindex: