Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:FILLer:TIME:EQUation
	single: SEQuence:ITEM:FILLer:TIME:FIXed
	single: SEQuence:ITEM:FILLer:TIME

.. code-block:: python

	SEQuence:ITEM:FILLer:TIME:EQUation
	SEQuence:ITEM:FILLer:TIME:FIXed
	SEQuence:ITEM:FILLer:TIME



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Filler.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: