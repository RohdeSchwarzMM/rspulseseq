Am
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:AM:DEPTh
	single: IMPort:PDW:DATA:AM:MODFreq

.. code-block:: python

	IMPort:PDW:DATA:AM:DEPTh
	IMPort:PDW:DATA:AM:MODFreq



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.Am.AmCls
	:members:
	:undoc-members:
	:noindex: