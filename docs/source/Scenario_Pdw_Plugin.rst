Plugin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:PDW:PLUGin:NAME

.. code-block:: python

	SCENario:PDW:PLUGin:NAME



.. autoclass:: RsPulseSeq.Implementations.Scenario.Pdw.Plugin.PluginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.pdw.plugin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Pdw_Plugin_Variable.rst