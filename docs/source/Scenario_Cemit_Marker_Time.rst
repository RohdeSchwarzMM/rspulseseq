Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:CEMit:MARKer:TIME:POST
	single: SCENario:CEMit:MARKer:TIME:PRE

.. code-block:: python

	SCENario:CEMit:MARKer:TIME:POST
	SCENario:CEMit:MARKer:TIME:PRE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Cemit.Marker.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: