ImportPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SETup:RFALign:IMPort

.. code-block:: python

	SETup:RFALign:IMPort



.. autoclass:: RsPulseSeq.Implementations.Setup.RfAlign.ImportPy.ImportPyCls
	:members:
	:undoc-members:
	:noindex: