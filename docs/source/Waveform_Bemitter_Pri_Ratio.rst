Ratio
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: WAVeform:BEMitter:PRI:RATio:MAXimum
	single: WAVeform:BEMitter:PRI:RATio:MINimum

.. code-block:: python

	WAVeform:BEMitter:PRI:RATio:MAXimum
	WAVeform:BEMitter:PRI:RATio:MINimum



.. autoclass:: RsPulseSeq.Implementations.Waveform.Bemitter.Pri.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex: