Hide
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:HIDE

.. code-block:: python

	PROGram:HIDE



.. autoclass:: RsPulseSeq.Implementations.Program.Hide.HideCls
	:members:
	:undoc-members:
	:noindex: