Adjustment
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Adjustment.AdjustmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.adjustment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Adjustment_Reload.rst