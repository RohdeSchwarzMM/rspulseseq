Lissajous
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:LISSajous:AMPX
	single: SCAN:LISSajous:AMPZ
	single: SCAN:LISSajous:FREQ
	single: SCAN:LISSajous:PHIX
	single: SCAN:LISSajous:PHIZ
	single: SCAN:LISSajous:XFACtor
	single: SCAN:LISSajous:ZFACtor

.. code-block:: python

	SCAN:LISSajous:AMPX
	SCAN:LISSajous:AMPZ
	SCAN:LISSajous:FREQ
	SCAN:LISSajous:PHIX
	SCAN:LISSajous:PHIZ
	SCAN:LISSajous:XFACtor
	SCAN:LISSajous:ZFACtor



.. autoclass:: RsPulseSeq.Implementations.Scan.Lissajous.LissajousCls
	:members:
	:undoc-members:
	:noindex: