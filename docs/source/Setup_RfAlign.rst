RfAlign
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SETup:RFALign:INSTrument
	single: SETup:RFALign:SETup

.. code-block:: python

	SETup:RFALign:INSTrument
	SETup:RFALign:SETup



.. autoclass:: RsPulseSeq.Implementations.Setup.RfAlign.RfAlignCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.setup.rfAlign.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Setup_RfAlign_ImportPy.rst