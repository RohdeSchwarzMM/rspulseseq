Path
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PROGram:PATH:CALCulated
	single: PROGram:PATH:INSTall
	single: PROGram:PATH:REPort
	single: PROGram:PATH:VOLatile

.. code-block:: python

	PROGram:PATH:CALCulated
	PROGram:PATH:INSTall
	PROGram:PATH:REPort
	PROGram:PATH:VOLatile



.. autoclass:: RsPulseSeq.Implementations.Program.Path.PathCls
	:members:
	:undoc-members:
	:noindex: