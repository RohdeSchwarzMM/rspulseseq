Fsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:FSK:DEViation
	single: PULSe:MOP:FSK:INVert
	single: PULSe:MOP:FSK:SRATe
	single: PULSe:MOP:FSK:TYPE

.. code-block:: python

	PULSe:MOP:FSK:DEViation
	PULSe:MOP:FSK:INVert
	PULSe:MOP:FSK:SRATe
	PULSe:MOP:FSK:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Fsk.FskCls
	:members:
	:undoc-members:
	:noindex: