Receiver
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:RECeiver:ANTenna
	single: SCENario:LOCalized:RECeiver:BM
	single: SCENario:LOCalized:RECeiver:GAIN
	single: SCENario:LOCalized:RECeiver:HEIGht
	single: SCENario:LOCalized:RECeiver:LATitude
	single: SCENario:LOCalized:RECeiver:LONGitude
	single: SCENario:LOCalized:RECeiver:SCAN

.. code-block:: python

	SCENario:LOCalized:RECeiver:ANTenna
	SCENario:LOCalized:RECeiver:BM
	SCENario:LOCalized:RECeiver:GAIN
	SCENario:LOCalized:RECeiver:HEIGht
	SCENario:LOCalized:RECeiver:LATitude
	SCENario:LOCalized:RECeiver:LONGitude
	SCENario:LOCalized:RECeiver:SCAN



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Receiver.ReceiverCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.receiver.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Receiver_Direction.rst
	Scenario_Localized_Receiver_Movement.rst