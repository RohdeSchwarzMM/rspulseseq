Waveform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:WAVeform:ANTenna
	single: SCENario:LOCalized:WAVeform:EIRP
	single: SCENario:LOCalized:WAVeform:FREQuency
	single: SCENario:LOCalized:WAVeform:LEVel
	single: SCENario:LOCalized:WAVeform:SCAN
	single: SCENario:LOCalized:WAVeform

.. code-block:: python

	SCENario:LOCalized:WAVeform:ANTenna
	SCENario:LOCalized:WAVeform:EIRP
	SCENario:LOCalized:WAVeform:FREQuency
	SCENario:LOCalized:WAVeform:LEVel
	SCENario:LOCalized:WAVeform:SCAN
	SCENario:LOCalized:WAVeform



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Waveform.WaveformCls
	:members:
	:undoc-members:
	:noindex: