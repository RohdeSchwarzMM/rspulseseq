ImportPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:MOVement:IMPort

.. code-block:: python

	SCENario:LOCalized:MOVement:IMPort



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Movement.ImportPy.ImportPyCls
	:members:
	:undoc-members:
	:noindex: