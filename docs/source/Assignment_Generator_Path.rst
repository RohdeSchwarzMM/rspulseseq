Path
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:GENerator:PATH:LIST
	single: ASSignment:GENerator:PATH:SELect

.. code-block:: python

	ASSignment:GENerator:PATH:LIST
	ASSignment:GENerator:PATH:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Generator.Path.PathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.generator.path.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Generator_Path_Antenna.rst
	Assignment_Generator_Path_Emitter.rst