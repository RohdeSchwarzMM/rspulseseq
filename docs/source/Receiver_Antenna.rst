Antenna
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: RECeiver:ANTenna:ALIas
	single: RECeiver:ANTenna:BM
	single: RECeiver:ANTenna:CLEar
	single: RECeiver:ANTenna:DELete
	single: RECeiver:ANTenna:GAIN
	single: RECeiver:ANTenna:PATTern
	single: RECeiver:ANTenna:SCAN
	single: RECeiver:ANTenna:SELect

.. code-block:: python

	RECeiver:ANTenna:ALIas
	RECeiver:ANTenna:BM
	RECeiver:ANTenna:CLEar
	RECeiver:ANTenna:DELete
	RECeiver:ANTenna:GAIN
	RECeiver:ANTenna:PATTern
	RECeiver:ANTenna:SCAN
	RECeiver:ANTenna:SELect



.. autoclass:: RsPulseSeq.Implementations.Receiver.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.receiver.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Receiver_Antenna_Add.rst
	Receiver_Antenna_Direction.rst
	Receiver_Antenna_Position.rst