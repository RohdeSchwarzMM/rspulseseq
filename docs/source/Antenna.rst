Antenna
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:CATalog
	single: ANTenna:COMMent
	single: ANTenna:CREate
	single: ANTenna:NAME
	single: ANTenna:REMove
	single: ANTenna:SELect

.. code-block:: python

	ANTenna:CATalog
	ANTenna:COMMent
	ANTenna:CREate
	ANTenna:NAME
	ANTenna:REMove
	ANTenna:SELect



.. autoclass:: RsPulseSeq.Implementations.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model.rst