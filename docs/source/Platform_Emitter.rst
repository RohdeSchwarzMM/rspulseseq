Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PLATform:EMITter:ALIas
	single: PLATform:EMITter:ANGLe
	single: PLATform:EMITter:AZIMuth
	single: PLATform:EMITter:BM
	single: PLATform:EMITter:BMID
	single: PLATform:EMITter:CLEar
	single: PLATform:EMITter:DELete
	single: PLATform:EMITter:ELEVation
	single: PLATform:EMITter:HEIGht
	single: PLATform:EMITter:RADius
	single: PLATform:EMITter:ROLL
	single: PLATform:EMITter:SELect
	single: PLATform:EMITter:X
	single: PLATform:EMITter:Y
	single: PLATform:EMITter

.. code-block:: python

	PLATform:EMITter:ALIas
	PLATform:EMITter:ANGLe
	PLATform:EMITter:AZIMuth
	PLATform:EMITter:BM
	PLATform:EMITter:BMID
	PLATform:EMITter:CLEar
	PLATform:EMITter:DELete
	PLATform:EMITter:ELEVation
	PLATform:EMITter:HEIGht
	PLATform:EMITter:RADius
	PLATform:EMITter:ROLL
	PLATform:EMITter:SELect
	PLATform:EMITter:X
	PLATform:EMITter:Y
	PLATform:EMITter



.. autoclass:: RsPulseSeq.Implementations.Platform.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.platform.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Platform_Emitter_Add.rst
	Platform_Emitter_BlankRanges.rst
	Platform_Emitter_Direction.rst