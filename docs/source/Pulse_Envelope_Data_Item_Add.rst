Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:ENVelope:DATA:ITEM:ADD

.. code-block:: python

	PULSe:ENVelope:DATA:ITEM:ADD



.. autoclass:: RsPulseSeq.Implementations.Pulse.Envelope.Data.Item.Add.AddCls
	:members:
	:undoc-members:
	:noindex: