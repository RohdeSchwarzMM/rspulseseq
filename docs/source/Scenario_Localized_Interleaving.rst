Interleaving
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:INTerleaving:MODE
	single: SCENario:LOCalized:INTerleaving:FREQagility
	single: SCENario:LOCalized:INTerleaving

.. code-block:: python

	SCENario:LOCalized:INTerleaving:MODE
	SCENario:LOCalized:INTerleaving:FREQagility
	SCENario:LOCalized:INTerleaving



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Interleaving.InterleavingCls
	:members:
	:undoc-members:
	:noindex: