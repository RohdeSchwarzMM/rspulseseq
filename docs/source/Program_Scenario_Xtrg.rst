Xtrg
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:SCENario:XTRG:ENABle

.. code-block:: python

	PROGram:SCENario:XTRG:ENABle



.. autoclass:: RsPulseSeq.Implementations.Program.Scenario.Xtrg.XtrgCls
	:members:
	:undoc-members:
	:noindex: