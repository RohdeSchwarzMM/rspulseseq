Reject
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:SETTings:REJect

.. code-block:: python

	PROGram:SETTings:REJect



.. autoclass:: RsPulseSeq.Implementations.Program.Settings.Reject.RejectCls
	:members:
	:undoc-members:
	:noindex: