Direction
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: RECeiver:ANTenna:DIRection:AWAY
	single: RECeiver:ANTenna:DIRection:AZIMuth
	single: RECeiver:ANTenna:DIRection:ELEVation

.. code-block:: python

	RECeiver:ANTenna:DIRection:AWAY
	RECeiver:ANTenna:DIRection:AZIMuth
	RECeiver:ANTenna:DIRection:ELEVation



.. autoclass:: RsPulseSeq.Implementations.Receiver.Antenna.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: