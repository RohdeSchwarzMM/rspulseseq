Noise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: WAVeform:NOISe:BWIDth

.. code-block:: python

	WAVeform:NOISe:BWIDth



.. autoclass:: RsPulseSeq.Implementations.Waveform.Noise.NoiseCls
	:members:
	:undoc-members:
	:noindex: