Quesation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STATus:QUESation:CONDition
	single: STATus:QUESation:ENABle
	single: STATus:QUESation:EVENt
	single: STATus:QUESation:NTRansition
	single: STATus:QUESation:PTRansition

.. code-block:: python

	STATus:QUESation:CONDition
	STATus:QUESation:ENABle
	STATus:QUESation:EVENt
	STATus:QUESation:NTRansition
	STATus:QUESation:PTRansition



.. autoclass:: RsPulseSeq.Implementations.Status.Quesation.QuesationCls
	:members:
	:undoc-members:
	:noindex: