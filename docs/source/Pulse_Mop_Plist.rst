Plist
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:PLISt:CLEar
	single: PULSe:MOP:PLISt:COUNt
	single: PULSe:MOP:PLISt:DELete
	single: PULSe:MOP:PLISt:INSert
	single: PULSe:MOP:PLISt:SELect
	single: PULSe:MOP:PLISt:VALue

.. code-block:: python

	PULSe:MOP:PLISt:CLEar
	PULSe:MOP:PLISt:COUNt
	PULSe:MOP:PLISt:DELete
	PULSe:MOP:PLISt:INSert
	PULSe:MOP:PLISt:SELect
	PULSe:MOP:PLISt:VALue



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Plist.PlistCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.plist.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Plist_Add.rst