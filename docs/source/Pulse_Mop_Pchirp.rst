Pchirp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:PCHirp:CLEar
	single: PULSe:MOP:PCHirp:COEFficient
	single: PULSe:MOP:PCHirp:COUNt
	single: PULSe:MOP:PCHirp:DELete
	single: PULSe:MOP:PCHirp:INSert
	single: PULSe:MOP:PCHirp:SELect
	single: PULSe:MOP:PCHirp:TERM

.. code-block:: python

	PULSe:MOP:PCHirp:CLEar
	PULSe:MOP:PCHirp:COEFficient
	PULSe:MOP:PCHirp:COUNt
	PULSe:MOP:PCHirp:DELete
	PULSe:MOP:PCHirp:INSert
	PULSe:MOP:PCHirp:SELect
	PULSe:MOP:PCHirp:TERM



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Pchirp.PchirpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.pchirp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Pchirp_Add.rst