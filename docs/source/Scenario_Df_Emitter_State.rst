State
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:EMITter:STATe:CLEar
	single: SCENario:DF:EMITter:STATe:COUNt
	single: SCENario:DF:EMITter:STATe:DELete
	single: SCENario:DF:EMITter:STATe:DURation
	single: SCENario:DF:EMITter:STATe:ENABle
	single: SCENario:DF:EMITter:STATe:INSert
	single: SCENario:DF:EMITter:STATe:SELect
	single: SCENario:DF:EMITter:STATe:VALue

.. code-block:: python

	SCENario:DF:EMITter:STATe:CLEar
	SCENario:DF:EMITter:STATe:COUNt
	SCENario:DF:EMITter:STATe:DELete
	SCENario:DF:EMITter:STATe:DURation
	SCENario:DF:EMITter:STATe:ENABle
	SCENario:DF:EMITter:STATe:INSert
	SCENario:DF:EMITter:STATe:SELect
	SCENario:DF:EMITter:STATe:VALue



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Emitter.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.emitter.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Emitter_State_Add.rst