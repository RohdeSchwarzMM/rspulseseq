Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:ADD

.. code-block:: python

	SCENario:LOCalized:ADD



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Add.AddCls
	:members:
	:undoc-members:
	:noindex: