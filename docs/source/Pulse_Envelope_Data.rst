Data
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:ENVelope:DATA:CLEar
	single: PULSe:ENVelope:DATA:LOAD
	single: PULSe:ENVelope:DATA:MULTiplier
	single: PULSe:ENVelope:DATA:OFFSet
	single: PULSe:ENVelope:DATA:SAVE
	single: PULSe:ENVelope:DATA:UNIT

.. code-block:: python

	PULSe:ENVelope:DATA:CLEar
	PULSe:ENVelope:DATA:LOAD
	PULSe:ENVelope:DATA:MULTiplier
	PULSe:ENVelope:DATA:OFFSet
	PULSe:ENVelope:DATA:SAVE
	PULSe:ENVelope:DATA:UNIT



.. autoclass:: RsPulseSeq.Implementations.Pulse.Envelope.Data.DataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.envelope.data.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Envelope_Data_Item.rst