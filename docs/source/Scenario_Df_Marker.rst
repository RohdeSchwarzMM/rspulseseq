Marker
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:MARKer:AUTO
	single: SCENario:DF:MARKer:FALL
	single: SCENario:DF:MARKer:FORCe
	single: SCENario:DF:MARKer:GATE
	single: SCENario:DF:MARKer:POST
	single: SCENario:DF:MARKer:PRE
	single: SCENario:DF:MARKer:RISE
	single: SCENario:DF:MARKer:WIDTh

.. code-block:: python

	SCENario:DF:MARKer:AUTO
	SCENario:DF:MARKer:FALL
	SCENario:DF:MARKer:FORCe
	SCENario:DF:MARKer:GATE
	SCENario:DF:MARKer:POST
	SCENario:DF:MARKer:PRE
	SCENario:DF:MARKer:RISE
	SCENario:DF:MARKer:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Marker_Time.rst