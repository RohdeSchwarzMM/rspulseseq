Destination
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ASSignment:DESTination:LIST
	single: ASSignment:DESTination:SELect

.. code-block:: python

	ASSignment:DESTination:LIST
	ASSignment:DESTination:SELect



.. autoclass:: RsPulseSeq.Implementations.Assignment.Destination.DestinationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.assignment.destination.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Assignment_Destination_Path.rst