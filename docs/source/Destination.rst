Destination
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DESTination:ADD
	single: DESTination:CLEar
	single: DESTination:COUNt
	single: DESTination:DELete
	single: DESTination:NAME
	single: DESTination:SELect

.. code-block:: python

	DESTination:ADD
	DESTination:CLEar
	DESTination:COUNt
	DESTination:DELete
	DESTination:NAME
	DESTination:SELect



.. autoclass:: RsPulseSeq.Implementations.Destination.DestinationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.destination.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Destination_Plugin.rst