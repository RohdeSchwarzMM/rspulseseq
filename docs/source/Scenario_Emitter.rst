Emitter
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:EMITter:CLEar
	single: SCENario:EMITter

.. code-block:: python

	SCENario:EMITter:CLEar
	SCENario:EMITter



.. autoclass:: RsPulseSeq.Implementations.Scenario.Emitter.EmitterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.emitter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Emitter_Direction.rst
	Scenario_Emitter_Mode.rst