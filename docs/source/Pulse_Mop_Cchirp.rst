Cchirp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:CCHirp:CLEar
	single: PULSe:MOP:CCHirp:COUNt
	single: PULSe:MOP:CCHirp:DELete
	single: PULSe:MOP:CCHirp:FREQuency
	single: PULSe:MOP:CCHirp:INSert
	single: PULSe:MOP:CCHirp:SELect

.. code-block:: python

	PULSe:MOP:CCHirp:CLEar
	PULSe:MOP:CCHirp:COUNt
	PULSe:MOP:CCHirp:DELete
	PULSe:MOP:CCHirp:FREQuency
	PULSe:MOP:CCHirp:INSert
	PULSe:MOP:CCHirp:SELect



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Cchirp.CchirpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.cchirp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Cchirp_Add.rst