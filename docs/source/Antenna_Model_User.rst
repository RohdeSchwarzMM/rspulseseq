User
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:USER:CLEar
	single: ANTenna:MODel:USER:LOAD

.. code-block:: python

	ANTenna:MODel:USER:CLEar
	ANTenna:MODel:USER:LOAD



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.User.UserCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.user.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_User_Csv.rst