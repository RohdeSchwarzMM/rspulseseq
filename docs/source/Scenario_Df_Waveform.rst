Waveform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:WAVeform:ANTenna
	single: SCENario:DF:WAVeform:EIRP
	single: SCENario:DF:WAVeform:FREQuency
	single: SCENario:DF:WAVeform:LEVel
	single: SCENario:DF:WAVeform:SCAN
	single: SCENario:DF:WAVeform

.. code-block:: python

	SCENario:DF:WAVeform:ANTenna
	SCENario:DF:WAVeform:EIRP
	SCENario:DF:WAVeform:FREQuency
	SCENario:DF:WAVeform:LEVel
	SCENario:DF:WAVeform:SCAN
	SCENario:DF:WAVeform



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Waveform.WaveformCls
	:members:
	:undoc-members:
	:noindex: