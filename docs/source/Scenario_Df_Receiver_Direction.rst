Direction
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:RECeiver:DIRection:PITCh
	single: SCENario:DF:RECeiver:DIRection:ROLL
	single: SCENario:DF:RECeiver:DIRection:YAW

.. code-block:: python

	SCENario:DF:RECeiver:DIRection:PITCh
	SCENario:DF:RECeiver:DIRection:ROLL
	SCENario:DF:RECeiver:DIRection:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Receiver.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: