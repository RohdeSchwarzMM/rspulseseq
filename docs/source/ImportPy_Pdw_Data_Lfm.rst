Lfm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IMPort:PDW:DATA:LFM:RATE

.. code-block:: python

	IMPort:PDW:DATA:LFM:RATE



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.Lfm.LfmCls
	:members:
	:undoc-members:
	:noindex: