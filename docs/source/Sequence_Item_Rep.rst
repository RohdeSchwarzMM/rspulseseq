Rep
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:REP:TYPE
	single: SEQuence:ITEM:REP:VARiable

.. code-block:: python

	SEQuence:ITEM:REP:TYPE
	SEQuence:ITEM:REP:VARiable



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Rep.RepCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sequence.item.rep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sequence_Item_Rep_Count.rst