HigHq
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SETup:HIGHq:ENABle
	single: SETup:HIGHq:MODE

.. code-block:: python

	SETup:HIGHq:ENABle
	SETup:HIGHq:MODE



.. autoclass:: RsPulseSeq.Implementations.Setup.HigHq.HigHqCls
	:members:
	:undoc-members:
	:noindex: