Carray
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:CARRay:GEOMetry
	single: ANTenna:MODel:CARRay:RESolution

.. code-block:: python

	ANTenna:MODel:CARRay:GEOMetry
	ANTenna:MODel:CARRay:RESolution



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Carray.CarrayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.antenna.model.carray.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Antenna_Model_Carray_Circular.rst
	Antenna_Model_Carray_Cosn.rst
	Antenna_Model_Carray_Distribution.rst
	Antenna_Model_Carray_Element.rst
	Antenna_Model_Carray_Hexagonal.rst
	Antenna_Model_Carray_Linear.rst
	Antenna_Model_Carray_Pedestal.rst
	Antenna_Model_Carray_Rectangular.rst