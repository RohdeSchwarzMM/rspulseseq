Pstep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCENario:LOCalized:RECeiver:MOVement:PSTep:SELect

.. code-block:: python

	SCENario:LOCalized:RECeiver:MOVement:PSTep:SELect



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Receiver.Movement.Pstep.PstepCls
	:members:
	:undoc-members:
	:noindex: