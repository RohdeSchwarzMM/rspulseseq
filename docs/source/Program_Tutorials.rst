Tutorials
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Program.Tutorials.TutorialsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.program.tutorials.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Program_Tutorials_Show.rst