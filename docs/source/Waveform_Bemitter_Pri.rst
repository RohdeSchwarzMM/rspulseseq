Pri
----------------------------------------





.. autoclass:: RsPulseSeq.Implementations.Waveform.Bemitter.Pri.PriCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.waveform.bemitter.pri.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Waveform_Bemitter_Pri_Ratio.rst