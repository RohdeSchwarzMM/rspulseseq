View
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:VOLatile:VIEW
	single: SCENario:VOLatile:VIEW:XMODe
	single: SCENario:VOLatile:VIEW:YMODe

.. code-block:: python

	SCENario:VOLatile:VIEW
	SCENario:VOLatile:VIEW:XMODe
	SCENario:VOLatile:VIEW:YMODe



.. autoclass:: RsPulseSeq.Implementations.Scenario.Volatile.View.ViewCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.volatile.view.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Volatile_View_Zoom.rst