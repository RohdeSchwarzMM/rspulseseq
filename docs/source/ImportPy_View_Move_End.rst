End
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IMPort:VIEW:MOVE:END

.. code-block:: python

	IMPort:VIEW:MOVE:END



.. autoclass:: RsPulseSeq.Implementations.ImportPy.View.Move.End.EndCls
	:members:
	:undoc-members:
	:noindex: