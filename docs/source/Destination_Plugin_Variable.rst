Variable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DESTination:PLUGin:VARiable:CATalog
	single: DESTination:PLUGin:VARiable:RESet
	single: DESTination:PLUGin:VARiable:VALue

.. code-block:: python

	DESTination:PLUGin:VARiable:CATalog
	DESTination:PLUGin:VARiable:RESet
	DESTination:PLUGin:VARiable:VALue



.. autoclass:: RsPulseSeq.Implementations.Destination.Plugin.Variable.VariableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.destination.plugin.variable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Destination_Plugin_Variable_Select.rst