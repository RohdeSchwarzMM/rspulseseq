AmMos
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:PDW:AMMos:AZIMuth
	single: SCENario:PDW:AMMos:FRAMe
	single: SCENario:PDW:AMMos:PPDW

.. code-block:: python

	SCENario:PDW:AMMos:AZIMuth
	SCENario:PDW:AMMos:FRAMe
	SCENario:PDW:AMMos:PPDW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Pdw.AmMos.AmMosCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.pdw.amMos.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Pdw_AmMos_Utime.rst