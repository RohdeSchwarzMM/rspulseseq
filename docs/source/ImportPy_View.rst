View
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IMPort:VIEW:COUNt

.. code-block:: python

	IMPort:VIEW:COUNt



.. autoclass:: RsPulseSeq.Implementations.ImportPy.View.ViewCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.importPy.view.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	ImportPy_View_Move.rst
	ImportPy_View_Time.rst