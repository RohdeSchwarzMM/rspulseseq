Move
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IMPort:VIEW:MOVE:STARt

.. code-block:: python

	IMPort:VIEW:MOVE:STARt



.. autoclass:: RsPulseSeq.Implementations.ImportPy.View.Move.MoveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.importPy.view.move.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	ImportPy_View_Move_Backwards.rst
	ImportPy_View_Move_End.rst
	ImportPy_View_Move_Forward.rst