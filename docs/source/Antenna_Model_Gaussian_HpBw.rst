HpBw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:GAUSsian:HPBW:AZIMuth
	single: ANTenna:MODel:GAUSsian:HPBW:ELEVation

.. code-block:: python

	ANTenna:MODel:GAUSsian:HPBW:AZIMuth
	ANTenna:MODel:GAUSsian:HPBW:ELEVation



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Gaussian.HpBw.HpBwCls
	:members:
	:undoc-members:
	:noindex: