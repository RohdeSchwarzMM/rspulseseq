Firing
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:LIST:FIRing:ENABle
	single: IPM:LIST:FIRing:SEQuence

.. code-block:: python

	IPM:LIST:FIRing:ENABle
	IPM:LIST:FIRing:SEQuence



.. autoclass:: RsPulseSeq.Implementations.Ipm.ListPy.Firing.FiringCls
	:members:
	:undoc-members:
	:noindex: