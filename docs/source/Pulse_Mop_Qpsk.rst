Qpsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:QPSK:SRATe
	single: PULSe:MOP:QPSK:TYPE

.. code-block:: python

	PULSe:MOP:QPSK:SRATe
	PULSe:MOP:QPSK:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Qpsk.QpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.qpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Qpsk_SoQpsk.rst