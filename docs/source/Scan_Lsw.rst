Lsw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:LSW:DIRection
	single: SCAN:LSW:DWELl
	single: SCAN:LSW:LOBes
	single: SCAN:LSW:ROTation
	single: SCAN:LSW:SQUint

.. code-block:: python

	SCAN:LSW:DIRection
	SCAN:LSW:DWELl
	SCAN:LSW:LOBes
	SCAN:LSW:ROTation
	SCAN:LSW:SQUint



.. autoclass:: RsPulseSeq.Implementations.Scan.Lsw.LswCls
	:members:
	:undoc-members:
	:noindex: