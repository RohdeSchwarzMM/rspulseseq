Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:EMITter:MODE:BEAM
	single: SCENario:DF:EMITter:MODE:TRACkrec
	single: SCENario:DF:EMITter:MODE

.. code-block:: python

	SCENario:DF:EMITter:MODE:BEAM
	SCENario:DF:EMITter:MODE:TRACkrec
	SCENario:DF:EMITter:MODE



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Emitter.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: