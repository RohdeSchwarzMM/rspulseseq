Step
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:STEP:BASE
	single: IPM:STEP:BURSt
	single: IPM:STEP:INCRement
	single: IPM:STEP:PERiod
	single: IPM:STEP:STARt
	single: IPM:STEP:STEPs

.. code-block:: python

	IPM:STEP:BASE
	IPM:STEP:BURSt
	IPM:STEP:INCRement
	IPM:STEP:PERiod
	IPM:STEP:STARt
	IPM:STEP:STEPs



.. autoclass:: RsPulseSeq.Implementations.Ipm.Step.StepCls
	:members:
	:undoc-members:
	:noindex: