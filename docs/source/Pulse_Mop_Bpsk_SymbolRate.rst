SymbolRate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:BPSK:SRATe:AUTO
	single: PULSe:MOP:BPSK:SRATe

.. code-block:: python

	PULSe:MOP:BPSK:SRATe:AUTO
	PULSe:MOP:BPSK:SRATe



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Bpsk.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: