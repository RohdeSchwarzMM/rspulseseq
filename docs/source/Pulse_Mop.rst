Mop
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:COMMent
	single: PULSe:MOP:ENABle
	single: PULSe:MOP:TYPE

.. code-block:: python

	PULSe:MOP:COMMent
	PULSe:MOP:ENABle
	PULSe:MOP:TYPE



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.MopCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Am.rst
	Pulse_Mop_AmStep.rst
	Pulse_Mop_Ask.rst
	Pulse_Mop_Barker.rst
	Pulse_Mop_Bpsk.rst
	Pulse_Mop_Cchirp.rst
	Pulse_Mop_Chirp.rst
	Pulse_Mop_Data.rst
	Pulse_Mop_EightPsk.rst
	Pulse_Mop_Exclude.rst
	Pulse_Mop_FilterPy.rst
	Pulse_Mop_Fm.rst
	Pulse_Mop_FmStep.rst
	Pulse_Mop_Fsk.rst
	Pulse_Mop_Msk.rst
	Pulse_Mop_NlCirp.rst
	Pulse_Mop_Noise.rst
	Pulse_Mop_Pchirp.rst
	Pulse_Mop_Piecewise.rst
	Pulse_Mop_Plist.rst
	Pulse_Mop_Plugin.rst
	Pulse_Mop_Poly.rst
	Pulse_Mop_Qam.rst
	Pulse_Mop_Qpsk.rst