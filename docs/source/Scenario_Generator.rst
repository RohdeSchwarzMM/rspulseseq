Generator
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:GENerator:CLEar
	single: SCENario:GENerator:PATH
	single: SCENario:GENerator

.. code-block:: python

	SCENario:GENerator:CLEar
	SCENario:GENerator:PATH
	SCENario:GENerator



.. autoclass:: RsPulseSeq.Implementations.Scenario.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex: