Lserver
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: LSERver:OPTions
	single: LSERver:READy
	single: LSERver:STATus

.. code-block:: python

	LSERver:OPTions
	LSERver:READy
	LSERver:STATus



.. autoclass:: RsPulseSeq.Implementations.Lserver.LserverCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lserver.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Lserver_Apply.rst