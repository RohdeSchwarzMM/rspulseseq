Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PULSe:MOP:AMSTep:ADD

.. code-block:: python

	PULSe:MOP:AMSTep:ADD



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.AmStep.Add.AddCls
	:members:
	:undoc-members:
	:noindex: