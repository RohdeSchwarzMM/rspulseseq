Movement
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:DF:RECeiver:MOVement:ACCeleration
	single: SCENario:DF:RECeiver:MOVement:ANGLe
	single: SCENario:DF:RECeiver:MOVement:ATTitude
	single: SCENario:DF:RECeiver:MOVement:CLEar
	single: SCENario:DF:RECeiver:MOVement:EAST
	single: SCENario:DF:RECeiver:MOVement:HEIGht
	single: SCENario:DF:RECeiver:MOVement:NORTh
	single: SCENario:DF:RECeiver:MOVement:PITCh
	single: SCENario:DF:RECeiver:MOVement:RFRame
	single: SCENario:DF:RECeiver:MOVement:RMODe
	single: SCENario:DF:RECeiver:MOVement:ROLL
	single: SCENario:DF:RECeiver:MOVement:SMOothening
	single: SCENario:DF:RECeiver:MOVement:SPEed
	single: SCENario:DF:RECeiver:MOVement:SPINning
	single: SCENario:DF:RECeiver:MOVement:TYPE
	single: SCENario:DF:RECeiver:MOVement:VEHicle
	single: SCENario:DF:RECeiver:MOVement:YAW

.. code-block:: python

	SCENario:DF:RECeiver:MOVement:ACCeleration
	SCENario:DF:RECeiver:MOVement:ANGLe
	SCENario:DF:RECeiver:MOVement:ATTitude
	SCENario:DF:RECeiver:MOVement:CLEar
	SCENario:DF:RECeiver:MOVement:EAST
	SCENario:DF:RECeiver:MOVement:HEIGht
	SCENario:DF:RECeiver:MOVement:NORTh
	SCENario:DF:RECeiver:MOVement:PITCh
	SCENario:DF:RECeiver:MOVement:RFRame
	SCENario:DF:RECeiver:MOVement:RMODe
	SCENario:DF:RECeiver:MOVement:ROLL
	SCENario:DF:RECeiver:MOVement:SMOothening
	SCENario:DF:RECeiver:MOVement:SPEed
	SCENario:DF:RECeiver:MOVement:SPINning
	SCENario:DF:RECeiver:MOVement:TYPE
	SCENario:DF:RECeiver:MOVement:VEHicle
	SCENario:DF:RECeiver:MOVement:YAW



.. autoclass:: RsPulseSeq.Implementations.Scenario.Df.Receiver.Movement.MovementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.df.receiver.movement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Df_Receiver_Movement_ImportPy.rst
	Scenario_Df_Receiver_Movement_Pstep.rst
	Scenario_Df_Receiver_Movement_Vfile.rst
	Scenario_Df_Receiver_Movement_Waypoint.rst