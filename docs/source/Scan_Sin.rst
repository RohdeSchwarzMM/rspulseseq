Sin
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCAN:SIN:HEIGht
	single: SCAN:SIN:INVersion
	single: SCAN:SIN:RATE
	single: SCAN:SIN:ROTation
	single: SCAN:SIN:UNIDirection
	single: SCAN:SIN:WIDTh

.. code-block:: python

	SCAN:SIN:HEIGht
	SCAN:SIN:INVersion
	SCAN:SIN:RATE
	SCAN:SIN:ROTation
	SCAN:SIN:UNIDirection
	SCAN:SIN:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Scan.Sin.SinCls
	:members:
	:undoc-members:
	:noindex: