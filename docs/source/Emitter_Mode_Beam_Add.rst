Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: EMITter:MODE:BEAM:ADD

.. code-block:: python

	EMITter:MODE:BEAM:ADD



.. autoclass:: RsPulseSeq.Implementations.Emitter.Mode.Beam.Add.AddCls
	:members:
	:undoc-members:
	:noindex: