Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:TIME:FALL
	single: PULSe:TIME:POST
	single: PULSe:TIME:PRE
	single: PULSe:TIME:REFerence
	single: PULSe:TIME:RISE
	single: PULSe:TIME:WIDTh

.. code-block:: python

	PULSe:TIME:FALL
	PULSe:TIME:POST
	PULSe:TIME:PRE
	PULSe:TIME:REFerence
	PULSe:TIME:RISE
	PULSe:TIME:WIDTh



.. autoclass:: RsPulseSeq.Implementations.Pulse.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: