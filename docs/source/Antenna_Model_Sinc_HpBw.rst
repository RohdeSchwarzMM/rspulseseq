HpBw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:SINC:HPBW:AZIMuth
	single: ANTenna:MODel:SINC:HPBW:ELEVation

.. code-block:: python

	ANTenna:MODel:SINC:HPBW:AZIMuth
	ANTenna:MODel:SINC:HPBW:ELEVation



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Sinc.HpBw.HpBwCls
	:members:
	:undoc-members:
	:noindex: