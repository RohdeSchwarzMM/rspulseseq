Csv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ANTenna:MODel:USER:CSV:FORMat

.. code-block:: python

	ANTenna:MODel:USER:CSV:FORMat



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.User.Csv.CsvCls
	:members:
	:undoc-members:
	:noindex: