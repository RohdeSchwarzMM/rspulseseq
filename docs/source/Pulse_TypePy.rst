TypePy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:TYPE:FALL
	single: PULSe:TYPE:RISE

.. code-block:: python

	PULSe:TYPE:FALL
	PULSe:TYPE:RISE



.. autoclass:: RsPulseSeq.Implementations.Pulse.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: