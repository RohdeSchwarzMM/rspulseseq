Zoom
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:VOLatile:VIEW:ZOOM:POINt
	single: SCENario:VOLatile:VIEW:ZOOM:RANGe

.. code-block:: python

	SCENario:VOLatile:VIEW:ZOOM:POINt
	SCENario:VOLatile:VIEW:ZOOM:RANGe



.. autoclass:: RsPulseSeq.Implementations.Scenario.Volatile.View.Zoom.ZoomCls
	:members:
	:undoc-members:
	:noindex: