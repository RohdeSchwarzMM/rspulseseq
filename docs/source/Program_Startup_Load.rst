Load
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROGram:STARtup:LOAD:ENABle

.. code-block:: python

	PROGram:STARtup:LOAD:ENABle



.. autoclass:: RsPulseSeq.Implementations.Program.Startup.Load.LoadCls
	:members:
	:undoc-members:
	:noindex: