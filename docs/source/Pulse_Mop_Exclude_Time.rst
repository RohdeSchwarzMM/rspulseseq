Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:EXCLude:TIME:STARt
	single: PULSe:MOP:EXCLude:TIME:STOP

.. code-block:: python

	PULSe:MOP:EXCLude:TIME:STARt
	PULSe:MOP:EXCLude:TIME:STOP



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Exclude.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: