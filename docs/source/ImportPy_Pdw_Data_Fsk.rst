Fsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IMPort:PDW:DATA:FSK:CHIPcount
	single: IMPort:PDW:DATA:FSK:PATTern
	single: IMPort:PDW:DATA:FSK:RATE
	single: IMPort:PDW:DATA:FSK:STATes
	single: IMPort:PDW:DATA:FSK:STEP

.. code-block:: python

	IMPort:PDW:DATA:FSK:CHIPcount
	IMPort:PDW:DATA:FSK:PATTern
	IMPort:PDW:DATA:FSK:RATE
	IMPort:PDW:DATA:FSK:STATes
	IMPort:PDW:DATA:FSK:STEP



.. autoclass:: RsPulseSeq.Implementations.ImportPy.Pdw.Data.Fsk.FskCls
	:members:
	:undoc-members:
	:noindex: