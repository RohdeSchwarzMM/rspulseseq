Condition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SEQuence:ITEM:MARKer:CONDition:TYPE
	single: SEQuence:ITEM:MARKer:CONDition:VALue
	single: SEQuence:ITEM:MARKer:CONDition:VARiable
	single: SEQuence:ITEM:MARKer:CONDition

.. code-block:: python

	SEQuence:ITEM:MARKer:CONDition:TYPE
	SEQuence:ITEM:MARKer:CONDition:VALue
	SEQuence:ITEM:MARKer:CONDition:VARiable
	SEQuence:ITEM:MARKer:CONDition



.. autoclass:: RsPulseSeq.Implementations.Sequence.Item.Marker.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: