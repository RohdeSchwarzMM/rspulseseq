Ripple
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:RIPPle:FREQuency
	single: PULSe:RIPPle

.. code-block:: python

	PULSe:RIPPle:FREQuency
	PULSe:RIPPle



.. autoclass:: RsPulseSeq.Implementations.Pulse.Ripple.RippleCls
	:members:
	:undoc-members:
	:noindex: