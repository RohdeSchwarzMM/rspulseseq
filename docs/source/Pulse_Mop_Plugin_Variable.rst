Variable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PULSe:MOP:PLUGin:VARiable:CATalog
	single: PULSe:MOP:PLUGin:VARiable:RESet
	single: PULSe:MOP:PLUGin:VARiable:VALue

.. code-block:: python

	PULSe:MOP:PLUGin:VARiable:CATalog
	PULSe:MOP:PLUGin:VARiable:RESet
	PULSe:MOP:PLUGin:VARiable:VALue



.. autoclass:: RsPulseSeq.Implementations.Pulse.Mop.Plugin.Variable.VariableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pulse.mop.plugin.variable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pulse_Mop_Plugin_Variable_Select.rst