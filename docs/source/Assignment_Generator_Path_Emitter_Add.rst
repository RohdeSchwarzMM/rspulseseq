Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ASSignment:GENerator:PATH:EMITter:ADD

.. code-block:: python

	ASSignment:GENerator:PATH:EMITter:ADD



.. autoclass:: RsPulseSeq.Implementations.Assignment.Generator.Path.Emitter.Add.AddCls
	:members:
	:undoc-members:
	:noindex: