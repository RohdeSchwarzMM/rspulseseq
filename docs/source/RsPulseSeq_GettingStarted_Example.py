"""Getting started - how to work with RsPulseSeq Python package.
This example performs basic RF settings on an R&S Pulse Sequence Software.
It shows the RsPulseSeq calls and their corresponding SCPI commands.
Notice that the python RsPulseSeq interfaces track the SCPI commands syntax."""

from RsPulseSeq import *

# Open the session
ps = RsPulseSeq('TCPIP::10.102.52.44::HISLIP', False, False)
# Greetings, stranger...
print(f'Hello, I am: {ps.utilities.idn_string}')

#   SOURce:FREQuency:FIXed 223000000
ps.source.frequency.cw.set_value(223E6)

ps.source.areGenerator.radar.base.set_attenuation(10)

# Close the session
ps.close()
