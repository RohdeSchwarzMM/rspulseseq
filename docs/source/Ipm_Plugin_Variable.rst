Variable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:PLUGin:VARiable:CATalog
	single: IPM:PLUGin:VARiable:RESet
	single: IPM:PLUGin:VARiable:VALue

.. code-block:: python

	IPM:PLUGin:VARiable:CATalog
	IPM:PLUGin:VARiable:RESet
	IPM:PLUGin:VARiable:VALue



.. autoclass:: RsPulseSeq.Implementations.Ipm.Plugin.Variable.VariableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ipm.plugin.variable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ipm_Plugin_Variable_Select.rst