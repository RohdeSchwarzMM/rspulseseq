Group
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:LOCalized:GROup:ALIas
	single: SCENario:LOCalized:GROup:CATalog
	single: SCENario:LOCalized:GROup:CLEar
	single: SCENario:LOCalized:GROup:COUNt
	single: SCENario:LOCalized:GROup:DELete
	single: SCENario:LOCalized:GROup:SELect
	single: SCENario:LOCalized:GROup

.. code-block:: python

	SCENario:LOCalized:GROup:ALIas
	SCENario:LOCalized:GROup:CATalog
	SCENario:LOCalized:GROup:CLEar
	SCENario:LOCalized:GROup:COUNt
	SCENario:LOCalized:GROup:DELete
	SCENario:LOCalized:GROup:SELect
	SCENario:LOCalized:GROup



.. autoclass:: RsPulseSeq.Implementations.Scenario.Localized.Group.GroupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.scenario.localized.group.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Scenario_Localized_Group_Add.rst