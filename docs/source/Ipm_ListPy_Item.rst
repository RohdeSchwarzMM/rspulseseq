Item
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: IPM:LIST:ITEM:COUNt
	single: IPM:LIST:ITEM:DELete
	single: IPM:LIST:ITEM:REPetition
	single: IPM:LIST:ITEM:SELect
	single: IPM:LIST:ITEM:TIME
	single: IPM:LIST:ITEM:VALue

.. code-block:: python

	IPM:LIST:ITEM:COUNt
	IPM:LIST:ITEM:DELete
	IPM:LIST:ITEM:REPetition
	IPM:LIST:ITEM:SELect
	IPM:LIST:ITEM:TIME
	IPM:LIST:ITEM:VALue



.. autoclass:: RsPulseSeq.Implementations.Ipm.ListPy.Item.ItemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ipm.listPy.item.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ipm_ListPy_Item_Add.rst