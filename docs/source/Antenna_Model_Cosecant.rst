Cosecant
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ANTenna:MODel:COSecant:HPBW
	single: ANTenna:MODel:COSecant:RESolution
	single: ANTenna:MODel:COSecant:T1
	single: ANTenna:MODel:COSecant:T2

.. code-block:: python

	ANTenna:MODel:COSecant:HPBW
	ANTenna:MODel:COSecant:RESolution
	ANTenna:MODel:COSecant:T1
	ANTenna:MODel:COSecant:T2



.. autoclass:: RsPulseSeq.Implementations.Antenna.Model.Cosecant.CosecantCls
	:members:
	:undoc-members:
	:noindex: