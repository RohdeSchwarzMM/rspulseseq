Backwards
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: IMPort:VIEW:MOVE:BACKwards

.. code-block:: python

	IMPort:VIEW:MOVE:BACKwards



.. autoclass:: RsPulseSeq.Implementations.ImportPy.View.Move.Backwards.BackwardsCls
	:members:
	:undoc-members:
	:noindex: