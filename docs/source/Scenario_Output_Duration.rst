Duration
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SCENario:OUTPut:DURation:AUTO
	single: SCENario:OUTPut:DURation:MODE
	single: SCENario:OUTPut:DURation:TIME

.. code-block:: python

	SCENario:OUTPut:DURation:AUTO
	SCENario:OUTPut:DURation:MODE
	SCENario:OUTPut:DURation:TIME



.. autoclass:: RsPulseSeq.Implementations.Scenario.Output.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex: